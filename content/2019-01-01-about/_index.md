---
title: About
type: page
last_update: 2020-09-07
---

## Maud Bausier (Brussels, 1991)

→ [CV & Folio](file:MaudBausier_2020.pdf)

## Background

I graduated in interior architecture, visual and space art in 2014 at [ESA Saint-Luc Brussels](http://www.stluc-bruxelles-esa.be/) and I have worked in a Brussels based [architecture office](http://www.ba-architectes.be) for 4 years where I did housing renovation and extension, from design to site supervision. I have explored the possibilities of doing something new by keeping the old (inspired by the wabi-sabi and kintsugi Japanese’s concepts) and the possibilities of restoring old constructive materials to service with the principle of a circular economy.

Also, during the same period, I have contributed to the [Open Structures](http://www.openstructures.family) project which explores the possibilities of modularity and open source into design practice with the use of a shared geometrical grid where anyone designs for everyone.  

Those experiences have sharpened my sense of creativity as well as my sense of management and responsibilities through technical challenges, customer and supplier relations and team working.

## Interest

In January 2019, I decided to open my horizon by integrating the [Waag](https://waag.org/) Fablab Amsterdam where I have developed my interest in open design, digital fabrication and open hardware. I have seen the maker movement as a way to create and explore (almost) everything and to give the possibility to anyone to learn by doing and to erase the borders between disciplines.

Indeed, at Waag, I also had the chance to spend a part of my time in their Open WetLab which is a place for bio-art, bio-design and do-it-together biology. I have learned there how to work with living things and I have been doing some researches by growing mycelium into different medium in order to find new bio-composite materials to design bio-based pieces.

## Focus

This is my readings about the city of Barcelona as a laboratory for innovation through digital transformation as a social change that made me want to move to Barcelona and take part in this movement in January 2020.

In February 2020, I joined the [Fab City Hub](http://bcn.fab.city/) which is working to understand, learn and experiment with projects aiming for urban self-sufficiency within food, energy and materials. Together, we are working to build [The BioLab Kitchen](/biolab-kitchen-intro.html) as a strategy for urban resilience.
