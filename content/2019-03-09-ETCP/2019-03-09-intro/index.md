---
title: Introduction
tags: furniture, modular-system, OpenStructures
---

## Introduction

Etcetera Project is an evolutionary furniture proposal.
This project was born as part of my studies and grew up with an internship at Thomas Lommée (founder of [OpenStructures](http://openstructures.net/)).

The main idea of OpenStructures is to combine the modularity of systems like LEGO or Meccano with the collaborative power of digital success stories like Wikipedia or Linux.
The ambition of OpenStructures is to create puzzles instead of static objects where every component fit in a common modular grid.

My proposal is part of the OpenStructures project :

![](etcp-concept01.jpg)

The constructive principle develops in 3 points:

- Use an infinitely renewable fastening system, the knot
- Allow a great possibility of hooks
- Define a palette of components that can be completed to infinity thanks to the sharing of measurements

![](etcp-concept02.jpg)
![](etcp-concept03.jpg)
![](etcp-concept04.jpg)
![](etcp-concept05.jpg)
![](etcp-concept06.jpg)
