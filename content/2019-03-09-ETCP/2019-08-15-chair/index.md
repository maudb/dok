---
title: Make a Chair
tags: furniture, modular-system, OpenStructures, Fablab-Amsterdam
featured: True
featured_image: ETCP-chair-15.jpg
excerpt: Took advantage of my time at Waag Fablab to go a step further with my Etcetera modular system by exploring new way of making it
---
![](large:ETCP-chair-15.jpg)
![](ETCP-chair-14.jpg)
![](ETCP-chair-12.jpg:flux)
![](ETCP-chair-13.jpg:flux)


The idea was to build something which needs to be strong to test the solidity of the [system](), so I made an ETCP Chair.

![](ETCP-chair-11.gif)
![](ETCP-chair-08.jpg)
![](ETCP-chair-09.jpg)

I used different kind of assembling to test the solidity

![old inside tyre to make the seat](ETCP-chair-07.jpg)
![bolds and nuts system](ETCP-chair-01.jpg)
![ropes system](ETCP-chair-02.jpg)
![3D printed system](ETCP-chair-03.jpg)
![3D printed system](ETCP-chair-04.jpg)
![3D printed system upgraded](ETCP-chair-05.jpg)
![3D printed system upgraded](ETCP-chair-06.jpg)
