---
title: ETC Project
tags: furniture, modular-system, OpenStructures
featured: True
featured_image: etcp-intro.jpg
excerpt: an evolutionary furniture proposal based on a shared geometrical grid for an exploration of open source modularity
---
