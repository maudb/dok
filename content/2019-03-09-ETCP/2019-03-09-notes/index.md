---
title: New futures
tags: personal-notes, Fablab-Amsterdam
---

## Do my project in a fablab way

- Use the CNC to make the wood component
- Use the 3D printer to make the rings

## Build something strong with my system to find the weaknesses

- a chair

## Find new ways to assembly

### Explore possibilities to have non straight assembling

- [assembly with old plastic bottles](https://www.micaellapedros.com/)
- [bio plastic card that you melt and reuse](http://www.formcard.com/about)

## Find new purpose

### Create an autonomous shelf with soft robotic and electronics to grow plants

See below a collect of interesting links:

- [low tech alternatives](http://lowtechlab.org/wiki/Accueil)
- [11 ways to live in total autonomy](https://www.youtube.com/watch?v=edYQAHTF0Mk)
- [a FabAcademy project from BE about hydropony](https://fabacademy.org/2019/labs/ulb/students/axel-cornu/projects/final-project.html)
- [a FabAcademy project from CN about hydropony](https://fabacademy.org/2019/labs/szoil/students/haiyan-su/projects/final-project/)
- [instruction to make hydropony system](https://www.instructables.com/workshop/hydroponics/projects/)
- [community of nomad farmers, chefs, engineers and artists ﻿striving for more autonomy in daily life](http://www.kaskantine.nl/programs-2019-en.html)
