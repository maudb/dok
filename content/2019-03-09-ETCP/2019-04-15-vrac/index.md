---
title: Vrac
tags: furniture, modular-system, OpenStructures, Fablab-Amsterdam
---

![Open Structure Grid engraved on a textile with the lasercuter](ETCP-vrac-03.jpg)
![detail of the Open Structure Grid engraved on a textile with the lasercuter](ETCP-vrac-04.jpg:flux)
![](ETCP-vrac-01.jpg)
![crash test of the 3D printed rings (yellow)](ETCP-vrac-02.jpg)
![assembly test, non right angle with meltable bio-plastic](ETCP-vrac-07.jpg)
![assembly test, non right angle with 3D printed pieces and meltable bio-plastic](ETCP-vrac-05.jpg:flux)
![assembly system, test with bolds and nuts](ETCP-vrac-06.jpg)
