---
title: Waste Composite
tags: waste-design, biocomposite, circular-economy, homelab
featured: True
featured_image: coffeeCup-01.jpg
excerpt: exploration of designing with food waste
---


I have been spending a bit of my CO-VID19 quarantine's time on designing with food waste in order to explore "creative food cycles" and to find interesting properties of different materials.

The recipes are from [materiom website](http://www.materiom.org/), I will complete soon all the details of my journey.

## Coffee Grounds composite

### CoffeeCup

![](coffeeCup-05.jpg)
![](coffeeCup-03.jpg)
![](coffeeCup-02.jpg)
![](coffeeCup-01.jpg)
![](coffeeCup-00.jpg)

### CoffeeBowl

![](coffeeBowl-07.jpg)
![](coffeeBowl-06.jpg)
![](coffeeBowl-05.jpg)
![](coffeeBowl-03.jpg)
![](coffeeBowl-02.jpg)
![](coffeeBowl-00.jpg)
![](coffeeBowl-01.jpg)

## Orange Peel composite

![](orange-01.jpg)
![](orange-02.jpg)

![](orange-03.jpg:flux)
![](orange-04.jpg)

## Eggs Sheel composite

![](moldPlate-01.jpg)
![](eggPlate-00.jpg)
![](eggPlate-01.jpg:flux)

## Black Tea composite

![](teaCup-01.jpg)
![](teaCup-02.jpg)
![](teaPlate-01.jpg:flux)

## Conclusion

I found some trouble with the drying part, it takes quite a long time (up to 5-10 days) and the shape tends to change during this process. It also becomes relatively smaller. I tried to let them into the mold a bit longer but then, molds appear and it would take too long to dry. It could be interesting to try to oven bake them, but i'm afraid that the shape will change too much and cracks, or to design a mold  with holes for drying.
