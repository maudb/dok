---
title:  Make your own Incubator v0.1
tags: biodesign, biohack, hacklab, arduino, electronics
---

# Teaming Up

To continue this journey of building my own incubator, I asked my partner in crime, Antoine J., to help me with the electronics as he has now a lot of new knowledges and skills thanks to his Fab Academy diploma ;).

I put here is some personal notes of the tests we made before we complete the v0.1 but you'll find more clearly on [his documentation --incubator V0.1](https://antoine.studio/incubator-v0-1.html) where we got to with this first version.

# Personal Notes

## The choice of the board

### arduino nano

spec: [store.arduino.cc/arduino-nano](https://store.arduino.cc/arduino-nano)

                         |
-------------------------|-----------------
Microcontroller 	       | ATmega328
Operating Voltage 	     | 5 V
Input Voltage 	         | 7-12 V
DC Current per I/O Pins  | 40 mA (I/O Pins)
Power Consumption 	     | 19 mA

VS

### NodeMCU Amica

spec: [www.make-it.ca/nodemcu-arduino](https://www.make-it.ca/nodemcu-arduino/nodemcu-details-specifications/)

                   |
-------------------|-----------
Microcontroller	   | ESP-8266
Operating Voltage	 | 3.3V
Input Voltage	     | 4.5V-10V


## List of materials

- Heating Pad [Adafruit 1481 - 5-12DC](https://www.adafruit.com/product/1481) (I ordered my one through a local supplier in Spain: [BricoGeek](https://tienda.bricogeek.com/))
- 12V Axial Fan (caught in the fablab)
- humidity and temperature sensor [DHT11](https://tienda.bricogeek.com/sensores-temperatura/986-sensor-de-humedad-y-temperatura-dht11.html) > [tuto](https://create.arduino.cc/projecthub/arcaegecengiz/using-dht11-b0f365)
- 2X MOSFET IRL540N (I bought in [Diotronic](https://diotronic.com/)) > [spec](https://www.alldatasheet.com/view.jsp?Searchword=IRL540N)
- diode (caught in the fablab)
- 3x 10K resistor (caught in the fablab)


### Fan 12V powered with 9V

- [electronics.stackexchange.com](https://electronics.stackexchange.com/questions/317632/can-i-run-a-12v-fan-off-of-a-9v-power-supply-for-led-lights-and-are-there-any-si)


### How to test a MOSFET with a multimeter

- [youtube.com](https://www.youtube.com/watch?v=RkWy1EirEu8)


### How to use a IRFZ44N MOSFET with 3.3V

- the problem you can get: [electronics.stackexchange.com/](https://electronics.stackexchange.com/questions/109128/why-is-my-n-channel-mosfet-getting-very-hot-and-the-power-it-provides-to-device)
- proposal of using a transistor to activate the IRZ44N MOSFET with 3.3V [instructables.com](https://www.instructables.com/MQTT-Mood-Lights-With-ESP32/)
- spec of the "SMALL SIGNAL NPN TRANSISTOR" needed: [media.digikey.com](https://media.digikey.com/pdf/Data%20Sheets/ST%20Microelectronics%20PDFS/PN2222A.pdf)
- (What is transistor: [electronicsdesignhq.com/transistors/](https://www.electronicsdesignhq.com/transistors/))
- or use only a Logic-level MOSFETs like the IRLZ44N instead of the IRFZ44N


### IRL540N MOSFET

- spec [www.alldatasheet.com/](https://www.alldatasheet.com/view.jsp?Searchword=IRL540N)


### Arduino IDE

<pre>
const int blue = 16;
const int green = 4;
const int red = 2;

const int pinHeat = 14;
int powerHeat = 99; // 0-99
int levelHeat = map(powerHeat, 0, 99, 0, 255);

const int pinFan = 12;
int powerFan = 85; // 80-99
int levelFan = map(powerFan, 0, 99, 218, 255);

#include &lt;dht11.h&lt;
const int DHT11PIN = 13;
dht11 DHT11;

int targetTemp = 28;
int tempRange = 2;

void setup() {
  Serial.begin(9600);
  pinMode(blue, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(red, OUTPUT);
  pinMode(pinHeat, OUTPUT);
  pinMode(pinFan, OUTPUT);
}


void loop() {

  int chk = DHT11.read(DHT11PIN);
  float humidity = (float)DHT11.humidity;
  float temperature = (float)DHT11.temperature;

   if (temperature <= targetTemp - tempRange) {
    analogWrite(pinHeat, levelHeat);
    analogWrite(pinFan, levelFan);
    Serial.print("Heat: on / ");
    digitalWrite(red, 1);
    digitalWrite(blue, 0);
    digitalWrite(green, 0);
  } else if (temperature >= targetTemp + tempRange) {
    analogWrite(pinFan, levelFan);
    analogWrite(pinHeat, 0);
    Serial.print("Heat: off / ");
    digitalWrite(blue, 1);
    digitalWrite(red, 0);
    digitalWrite(green, 0);
  } else {
    analogWrite(pinFan, 0);
    analogWrite(pinHeat, 0);
    digitalWrite(green, 1);
    digitalWrite(red, 0);
    digitalWrite(blue, 0);
  }

  Serial.println();

  Serial.print("Humidity (%): ");
  Serial.println(humidity);

  Serial.print("Temperature (C): ");
  Serial.println(temperature);

  Serial.print("Target Temp (C): ");
  Serial.println(targetTemp);  

  delay(5000);  
}
</pre>


## Interface

- [figma.com](https://www.figma.com/)
- [reddit.com/nodemcu_wifisetup_web_interface/](https://www.reddit.com/r/esp8266/comments/3zykrk/nodemcu_wifisetup_web_interface/)
- [randomnerdtutorials.com/esp8266-web-server/](https://randomnerdtutorials.com/esp8266-web-server/)
- [tttapa.github.io/ESP8266/Simple-Web-Server.html](https://tttapa.github.io/ESP8266/Chap10%20-%20Simple%20Web%20Server.html)
- [lastminuteengineers.com/creating-esp8266-web-server-arduino-ide/](https://lastminuteengineers.com/creating-esp8266-web-server-arduino-ide/)


### Sketch Arduino ide

<pre>
#include &lt;ESP8266WebServer.h&gt;
ESP8266WebServer server(80);

// WiFi AP Config
IPAddress localIp(192,168,1,1);
IPAddress gatewayIp(192,168,1,1);
IPAddress subnet(255,255,255,0);
const char *ssid="Tempeh Maker";
const char *pwd="hola hola";

// LED
#define LED_PIN 6

void setup() {
  // Setup Wifi
  Serial.begin(9600);
  Serial.println("Initializing AP...");
  if (WiFi.softAP(ssid, pwd)) {
    Serial.println("AP ready....");
    WiFi.softAPConfig(localIp, gatewayIp, subnet);
    delay(200);

  }
  server.on("/", handle_home_page);
  server.on("/led/on", handle_led_on);
  server.on("/led/off", handle_led_off);
  server.begin();
}

void handle_led_on() {
  digitalWrite(LED_PIN, HIGH);
}

void handle_led_off() {
  digitalWrite(LED_PIN, LOW);
}

void handle_home_page() {
  char body[1024];
  sprintf(body,  "<html><head><title>ESP8266</title><meta name='viewport' content='width=device-width, initial-scale=1.0'></head><body><h1>ESP8266</h1><a href='/led/on'>ON</a><a href='/led/off'>OFF</a></body></html>");
  server.send(200, "text/html", body);
}

void loop() {
  server.handleClient();
  </pre>
