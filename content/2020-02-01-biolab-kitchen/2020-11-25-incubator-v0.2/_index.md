---
title:  Make your own Incubator v0.2
last_update: 2021-02-10
tags: biodesign, biohack, hacklab, arduino, electronics
featured: True
featured_image: incub-cab00.JPG
excerpt: First version of my MIY incubator
---

# Here we are!!

After a long journey through series of tutorials, tests, fails, new tests, we completed the first satisfying version of our own Make-It-Yourself incubator.


# Electronics

Antoine was in charge of making the electronics. He detailed everything through [his documentation page --incubator v0.2](https://antoine.studio/incubator-v0-2.html).

![The system is composed by a 12V fan, a 5-12V heating pad, a DHT11 humidity and temperature sensor, a RGB LED and the Barduino (an ESP32 development board made in Barcelona) with a custom PCB shield we made to plug on it.](incub-elec00.JPG:flux)

![Some components are soldered on the back of the PCB to ensure the robustness and therefore the longevity of the design. These are the Barduino's header, the barrel jack and the three terminals (heating pad, fan, DHT11 sensor).](incub-elec01.JPG:flux)

![The PCB is designed by Antoine, fabricated at Fablab BCN using a Roland SRM-20 milling machine and soldered at home by Antoine.](incub-elec02.JPG:flux)

![Then, we designed and 3D printed an enclosure for this marvellous electronics.](incub-case00.JPG:flux)

![How we arranged the electronic components inside the incubator](incub-elec03.JPG)



# Cabinet

I could then focus myself on the second step, make the cabinet.

There are several known ways to build an incubation cabinet: you can use a decommissioned refrigerator, a speed rack with a vinyl cover, styrofoam coolers, or wooden boxes. The two basic criteria for a good container are insulation and water resistance.

When we were working with the prototypes, we were using a styrofoam cooler box that I picked up (for free) in a fish shop. It was working well but we wanted to take this opportunity to make something ourselves, with the size we want and the shape we like, and have our tailored incubator as it is the main advantage of doing things yourself.

![The Noma Restaurant's Fermentation Chamber made with a Covered Speed Rack](small:noma_FermentationChamber_02.png)
![The Noma Restaurant's Fermentation Chamber made out of a styrofoam cooler](small:noma_FermentationChamber_01.png)
![Our previous incubator made out of a styrofoam cooler](small:incub-v01-01.JPG)
![Our previous incubator made out of a styrofoam cooler](small:incub-v01-02.JPG)

For this version, we choose to make a box out of plywood (15mm) for the following reasons:

- we could have access to the CNC milling machine of Fablab Barcelona to manufacture it
- we could then design it from scratch
- by using a parametric software
- and easily distribute the files with few instructions  

We came with a simple and playful design which assembles by itself with T-bones joints (no glue, no screws), except for the front door which uses 3D printed hinges and a 3D printed lock.

![](incub-cab03.GIF)

![sides (X2)](small:incub-plan01.png)
![top and bottom (X2)](small:incub-plan03.png)
![back](small:incub-plan02.png)
![door](small:incub-plan04.png)
![It took ~30 min to mill the panel](small:incub-cab01.jpg)
![](small:incub-cab02.jpg)
![](small:incub-cab04.JPG)
![](small:incub-cab03.JPG)
![](small:incub-cab11.JPG:flux)
![](small:incub-cab10.JPG:flux)
![](small:incub-cab07.JPG)

![](large:incub-elec04.JPG)
![](large:incub-cab06.JPG)
![](large:incub-cab00.JPG:flux)


I just sprayed varnish on the inside of the cabinet to make it easier to clean. So far, the cabinet is working like that but we observed that it took it more time to warm up till 30°C than with the styrofoam cooler. Which is normal because the cabinet's volume is larger, but we want to optimise it by insulating it from the inside. One of the idea would be to grow a thin layer of mycelium on its inner sides :-P, but we first gonna try with some ready to use natural insulation such as hemp or so.

![](incub-cab05.JPG)


# Tempeh

In the meantime, we continue to use it to make [tempeh](food-processes-tempeh.html)!

![](large:incub-cab09.JPG)

![black and white soy beans tempeh](small:tempeh-00.JPG)
![black and white soy beans tempeh](small:tempeh-02.JPG)
![black and white soy beans tempeh](small:tempeh-01.JPG)
![chickpeas tempeh](small:tempeh-03.JPG)
![lunchbowl with chickpeas tempeh](small:tempeh-04.JPG)


# Workshop

As a release but also to celebrate, we organised a tempeh workshop with some friends. We cooked different kinds of beans in advance and let everyone create their own tempeh, from the mix to the shape. We incubated them for about 30 hours and had lunch together two days later for tempeh tasting.

![White soy bean cooked and dried as the tempeh base](workshop-02.JPG)
![From the mix..](large:workshop-03.JPG)
![..to the shape](large:workshop-04.JPG)
![Everyone's tempeh after incubation](workshop-05.JPG)
![Degustation!](workshop-06.JPG)
