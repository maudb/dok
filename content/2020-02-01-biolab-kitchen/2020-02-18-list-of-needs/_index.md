---
title:  List of needs
tags: biodesign, biohack, homelab
last_update: 2020-11-04
---

![Kay Aull when she was a PhD student at the University of California. For a ridiculously low price, Aull set up a laboratory in her closet in her apartment in Boston.](closetlab.png)

# BioSafety Level 1

- work with well-characterized agents which do not cause disease in healthy humans
- wash hands upon entering and exiting the lab
- Research with these agents may be performed on standard open laboratory benches without the use of special containment equipment
- eating and drinking are prohibited in laboratory areas
- Potentially infectious material must be decontaminated before disposal, either by adding a chemical such as bleach or isopropanol or by packaging for decontamination elsewhere (autoclave)
- Personal protective equipment is only required for circumstances where personnel might be exposed to hazardous material
- BSL-1 laboratories must have a door which can be locked to limit access to the lab. However, it is not necessary for BSL-1 labs to be isolated from the general building

To know more about it :

- [BioSafety Levels](https://en.wikipedia.org/wiki/Biosafety_level)
- [BioSafety Rules](https://en.wikipedia.org/wiki/Biosafety) vs [BioSecurity Rules](https://en.wikipedia.org/wiki/Biosecurity)

# Additional Lab Safety Rules

- hair pulled back
- wear closed shoes
- wear lab coat
- (wear googles)
- (wear gloves)
- labeling everything (name, date, content)
- seal everything with parafilm
- store them in the appropriate emplacement
- clean everything after use


# Suppliers (in Barcelona)

- [Pidiscat](https://www.pidiscat.cat/ca/) (Poblenou)
- [Quimics Dalmau](https://quimicsdalmauonline.com/) (Eixample)
- find a second hand stock
- you can also find most of the things on an online shop to make your own cosmetic like [Gran Velada](https://www.granvelada.com/es/) in Catalunya or [Mi Cosmetica Casera](https://www.micosmeticacasera.es/) (South-West Spain) which is sometimes more affordable than professional shops.  


# First basics we need

MATERIALS |WORKSPACE |STERILISE |GROW      |STORE
----------|----------|----------|----------|-----
pans |easy-to-clean work bench |autoclave (DIY with a pressure cooker and hot plate) |incubator (DIY or reptilium) |sealed boxes|
glass bottle |gaz burner |autoclave bags and tape | | fridge (could be a sealed box into the main fridge)
petri dishes |ethanol 70% and wash bottle | | | freezer (same)
flasks |paper towel | | | |
beakers |microwave | | | |
funnel |oven | | | |
lab scale |deshydrator | | | |
thermometer |disinfectant gel | | | |
latex potholder |lab coat | | | |
spoons | safety rules displayed | | | |
small kitchen  |access to the water | | | |
scissors | a DIY laminar flow or a clean transparent box | | | |
tweezers | | | | |
pipettes | | | | |
inoculation loop spreader | | | | |
etc | | | | |

<!--
  MATERIALS:
  - pans, glass bottle, petri dishes, flasks, beakers, funnel, lab scale, thermometer, latex potholder, spoons, small kitchen knifes, scissors, tweezers, pipettes, inoculation loop spreader, etc
  - Parafilm, label, permanent pen
  WORKSPACE:
  - easy-to-clean work benches
  - gaz burner
  - ethanol 70% and a wash bottle
  - paper towel
  - microwave
  - oven
  - deshydrator
  - disinfectant gel
  - lab coat
  - safety rules displayed
  - access to water
  - a DIY laminar flow or a clean transparent box to work
  STERILISE:
  - Autoclave : Pressure cooker + hot plate
  - autoclave bags and tape
  GROW:
  - Incubator (DIY one or reptilium)
  STORE:
  - sealed boxes to store all the stuff
  - fridge (could be a sealed box into the main fridge)
  - freezer (same)
-->


# Usefull links

## BioHacking:

- [BioHack Academy github](https://biohackacademy.github.io/)
- [DIY Bio](https://diybio.org/) Community of DIY biologists
- [MIT Media Lab](https://www.media.mit.edu/) interdisciplinary research lab that encourage the unconventional mixing and matching of seemingly disparate research areas
- [Bio Summit](https://www.biosummit.org/)
- [Hackteria](http://www.hackteria.org/) webplatform and collection of Open Source Biological Art
- [Planet B.io](https://www.biotechcampusdelft.com/en/organisations/planet-b-io/) scale-up ecosystem focused on industrial biotechnology in Delft BioTech Campus
- [BioFabForum](https://biofabforum.org/) Bio Fabrication recipes and advices
- [BioFabForum: Links by topic](https://biofabforum.org/t/links-by-topic-in-one-document/215)
- [Open Source laminar flow cabinet](https://waaglfc.tumblr.com/)

## Bio-materials and bio-techniques:

- [Materiom](https://materiom.org/) Nature's Recipe online Book
- [Algae Lab](https://algae-lab.com/) GB supplier
- [BioShades](https://bioshades.bio/) Textile Dyeing with bacteria
- [Textile Academy wiki](https://class.textile-academy.org/)
- [GreenLab Blog](https://www.greenlab.org/blog/) London-based Green Lab

## Biodesigners and bio-artists:

- [Magical contamination](https://magical-contamination.tumblr.com/)
- [STARTS Prize 2020](https://starts-prize.aec.at/index.html) Science + Technology + Arts
- [Journey to the Microcosmos](https://www.youtube.com/channel/UCBbnbBWJtwsf0jLGUwX5Q3g/videos) youtube channel
- [Ginko Bioworks](https://www.ginkgobioworks.com/) Boston-based biotech company founded in 2009 by scientists from MIT
- [Faber Futures](https://faberfutures.com/) London-based award-winning futures agency operating at the intersection of nature, design, technology and society
- [biofriction](https://biofriction.org/)research project with the goal of generating and facilitating spaces for exchange
- [Cultivamos Cultura](https://cultivamoscultura.com/) is a platform for experimentation and development of shared knowledge in the theory and practice of science, technology and contemporary art
- [Quimera Rosa](https://quimerarosa.net/) Nomadic lab that researches and experiments on body, technoscience and identities
- [Atelier Luma](https://atelier-luma.org/) creates new and sustainable ways of using the natural and cultural resources of the bioregion
- [BioBabes](https://www.biobabes.co.uk/) Feminist collective of biodesigners, makers, and biohackers
- [Jannis](http://www.jannis.world/) designer, material researcher, and a lot more
- [Plasticula](https://www.plasticula.com) Turning plastic into compostable material
- [Circology](https://www.circology.org/) Material research and circular design studio
- [dezeen's Biofabrication posts](https://www.dezeen.com/tag/biofabrication/)
- [Loes Bogers (Textile Academy 2020) ](https://class.textile-academy.org/2020/loes.bogers/projects/archiving_new_naturals/) "Archiving new naturals"
- [Bela Rofe (Textile Academy 2020)](https://class.textile-academy.org/2020/bela.rofe/projects/GAIA/) "An interwoven tapestry of algae and female human hair"
- [Beatriz Sandini (Textile Academy 2020)](https://class.textile-academy.org/2020/beatriz.sandini/projects/0-final-project/) "The Ephemeral Fashion Lab"
- [MaDE (Material Designers)](http://materialdesigners.org/) is competition, event series and platform devoted to realising the positive impact material designers can have across all creative sectors.
- [Creative Food Cycles](https://creativefoodcycles.org/) explores new way of Food-Art-Creativity
- [Organic Matters](https://www.instagram.com/_organicmatters/) connecting local producers, material designers and industry to create compostable and regenerative applications
- [Cristina Noguer](http://cristinanoguer.com/) «Be loyal to ecology (from Greek: οἶκος, «house», or «environment»; -λογία, «study of») is the guiding principle of my research.»
- [Lara Campos](https://lara-campos.com/) "In a context of environmental collapse, design can open a space for dialogue between humans and other living beings."

## Mycelium

More on [Grow Mycelium - Library](grow-mycelium-library.html)'s page of this website


# Schedule

1. List of needs and costs
2. First equipment: incubator
3. Second equipment: a sterile workplace
4. Dedicate and arrange a place for the BioLab Kitchen
5. Play and upgrade little by little
