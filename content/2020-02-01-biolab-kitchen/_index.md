---
title: BioLab Kitchen
tags: biodesign, biohack, homelab
featured: True
featured_image: BioLabKitchen-incubator02.jpg
excerpt: guide to create a "BioHack Set Up" in order to give the possibility to everyone to explore creative natural cycles
---

## The Fab City model and The Fab City Hub Barcelona

From February 2020, I'm part-time hosted by the [Fab City Hub](https://bcn.fab.city/) in Poblenou (Barcelona) to develop projects.

![Carrer de Pujades 127, 08005 Barcelona](large:sign-close-up.jpg)

The Fab City Hub is part of a bigger vision – the Fab City model. The Fab City model has challenged Barcelona, amongst other cities in the world, to become active participants in producing everything consumed by 2054. The Barcelona's Hub is working to understand, learn and experiment with projects aiming for urban self-sufficiency within food, energy and materials. The Hub is using the productive potential of Poblenou district, to become locally produced and globally connected.

![The Fab City model - Fablab BCN](blogfabcity.png)

To contribute to this movement, I am working to build **The BioLab Kitchen**. The aim of the project is to create a “BioHack Set Up” that anybody can easily build and use at home in order to give the possibility to everybody to reimagine all aspects of food, waste and living systems for an emerging bio-circular economy and for a bio-based future. It is about going from an extractive economy to a regenerative economy and it is questioning the way of producing and the mass production model in which we are. It is also about observing and understanding natural systems for more synergies.


## The biohacking as a driver for urban resilience

As the hacking is a growing movement from the beginning of the domestic computers, it has already shown the potential to empower mass and individuals to create smart devices for themselves with, among others, the free and open-source movement, the maker culture, the open-source hardware, the FabLab movement and the DIY movement.

On the other side, the biological researches are still very conventional because the professional tools are expensive and only big private structures or institutions can afford it and they are then looking for specific results, influenced by lobbying or even corrupted.

But if we change the model, new results can appear. I believe in the hacking movement as a way to give to people the opportunity to create and explore (almost) everything and to give the possibility to anyone to learn by doing and to erase the borders between disciplines. On that way, cross-disciplinary collaboration and creativity supported by scientific research will expand, powered by global imperatives such as the urgency to develop and implement cleaner technologies. According to me, this convergence of fields, as well as the expert with the amateur, is necessary to support the ongoing effort to go against the negative impacts of the legacies of the Industrial Revolution. It will lead until the reconception of growth, sustainability and the primary design principles which is value generation. I think that the challenge of changing the story is immense, and it begin with education and awareness.

I am then developing the BioLab Kitchen as a strategy for challenges and opportunities related to future resilience, transparency and equity.

With the BioLab Kitchen we will define and document what is needed to start a biohack set-up from the BioSafety rules to the list of needs and costs, suppliers and recipes to learn by doing.

As we want to contribute in the change, we are working to keep all that informations accessible and open source as working within a community shows better and faster results and seems to be a key to resolve emergency problems.
