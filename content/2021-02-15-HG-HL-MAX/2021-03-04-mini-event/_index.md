---
title: Mini-Event
tags: creativeeurope, makersxchange, circular-economy
last_update: 2021-03-15
featured: True
featured_image: HGHL-fin-01.JPG
excerpt: Results and contributions following my involvement into the "Hyper Global / Hyper Local" Makers' eXchange, a pilot policy project co-funded by the European Union
---

![](HGHL-fin-01.JPG)
![](large:HGHL-fin-02.JPG)
![](large:HGHL-fin-03.JPG)
![](large:HGHL-fin-04.JPG)
![](large:HGHL-fin-05.JPG)
![](large:HGHL-fin-06.JPG)
![](large:HGHL-fin-07.JPG)
![](large:HGHL-fin-08.JPG)
![](large:HGHL-fin-09.JPG)
![](large:HGHL-fin-10.JPG)
![](large:HGHL-fin-11.JPG)
![](large:HGHL-fin-12.png)
![](large:HGHL-fin-13.png)
![](large:HGHL-fin-14.png)
![](large:HGHL-fin-15.png)

Last but not least, Julia behind the scene!

![](HGHL-fin-16.JPG)
