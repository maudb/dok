---
title: Hyper Global / Hyper Local Makers'exchange
tags: creativeeurope, makersxchange, circular-economy
last_update: 2021-03-15
featured: True
featured_image: HGHL-Julia02.JPG
excerpt: Co-designing and co-developing an open product that stimulates research and innovation in circular design by mixing different approaches, exploring new aesthetics and change our relationship with waste and extend the limits of modularity.
---

![My friend and partner Julia, hunting together for good resources.](HGHL-Julia01.JPG)
![On our way to the workshop!](HGHL-Julia02.JPG)

# Intro

My friend [Julia](https://juliabertolaso.com/) and I are currently developing a project with which we've applied and been selected to the [Hyper Global / Hyper Local - Makers' eXchange](https://makersxchange.eu/hyper-global-hyper-local-open-call/), a pilot policy project co-funded by the European Union.

Because we are two spatial designers with social and sustainable concerns, interested in crafts, emerging technologies and the circular economy, we applied with the aim to do research on these three topics through the production of objects by using construction materials considered as waste (such as wood, metal, etc.) and looking for ways to assemble them using both craft techniques (carpentry and other methods) and emerging techniques (digital and other tools). The idea is to mix different approaches and explore new aesthetics to change our relationship with waste and to extend the limits of modularity.

Through this project, we are exploring the possibilities of modularity with open-source principles in design practice, using a shared geometric grid created by [OpenStructures](https://www.openstructures.net/), to create various designs that can be used and re-adapted by anyone for their own use in collaboration with [TMDC (Taller para la Materialización y el Desarrollo de (grandes) Conceptos)](https://www.tmdc.es/) for the use of their workshop and waste material.

# Our proposal

The aim is to take part in a global open source movement to change the way we create, produce, and distribute products. The goal is to co-design and co-develop an open-source product that stimulates research and innovation in circular design.

- We are investigating open source hardware ideas on how to create a new product with factory waste through a systematic process of material research.
- We are focusing on developing functional prototypes.
- We are working to share technical documentation of the manufacturing of the product (drawings, assembly instructions, file formats) and the process behind it, using a website which will be developed for this project and the resources will be made available on GitLab

# Our partners

To do so, we are closely collaborating with **TMDC**(ES) and **Open Structures**(BE).

[TMDC](https://www.tmdc.es/) is an ecosystem of designers, engineers, manufacturers and architects, which is formed in different ways to carry out the different projects that are requested of them. They add to this community a workshop of more than 2000m2, and all types of machinery that make them the largest open workshop in Barcelona and possibly in Spain.

From TMDC, we will use the scrap material they produced (such as wood waste from the CNC or from the manual cut off, varying from sheets and solid materials to possibly dust, and eventually some other waste of construction material) and the facilities of their workshop and from their ecosystem.

Considering that the most important waste created in TMDC is sawdust, we might find the necessity to explore ways of materialising this wood composite, possibly using mycelium in this process.

[OpenStructures](https://www.openstructures.net/) is an exploration on open modular construction where anyone designs for everyone on the basis of one shared grid.

With Open Structures, we will use (and eventually generate parts) of their database to allow infinite adaptation and repair, and a more sustainably built environment. We will imagine solutions together, at a moment in time in which resources are becoming increasingly scarce and change is the new status quo, in which everybody is connected to everybody and everything can be produced everywhere.


# Our goal

We believe that the project can have an educational and social dimension, and serve as an example for

- resource management projects,
- creative workshops,
- as well as generating a discussion in relation to the current consumption model.

We wish to act as critical makers, taking a local and global challenge by exploring a solution to current social and environmental problems found within workshops and maker spaces.
