---
title: 3-week exchange Report
tags: creativeeurope, makersxchange, circular-economy
last_update: 2021-03-15
featured: True
featured_image: HGHL_finalpresentation_1.jpg
excerpt: Report of my involvement into the "Hyper Global / Hyper Local" Makers' eXchange, a pilot policy project co-funded by the European Union
---

## OpenStructures -Brussels x Maud Bausier -Barcelona

![slide 1](HGHL_finalpresentation_1.jpg)
![slide 2](HGHL_finalpresentation_2.jpg)
![slide 3](HGHL_finalpresentation_3.jpg)
![slide 4](HGHL_finalpresentation_4.jpg)
![slide 5](HGHL_finalpresentation_5.jpg)

A brief reminder of the challenge:

- To explore the implementation of OpenStructures open modular design methodology, using the waste materials produced by makerspaces such as TMDC.
- Through this project we aim to change our relationship to this type of waste, by exploring new aesthetics for OS_parts and engaging makers with an OS approach to extend the limits of modularity and come a step closer to circularity.

## The start   

![slide 6](HGHL_finalpresentation_6.jpg)
![slide 10](HGHL_finalpresentation_10.jpg)
![slide 12](HGHL_finalpresentation_12.jpg)
![slide 14](HGHL_finalpresentation_14.jpg)
![slide 15](HGHL_finalpresentation_15.jpg)
![slide 16](HGHL_finalpresentation_16.jpg)

- We familiarised with the space and got to know the community there.
- And as we started working at TMDC, seeing there was barely any scrap materials the focus changed from scrap material to sawdust.
  - The biggest amount of waste produced at TMDC is sawdust (With about 1 cubic meter every day)
  - The quality of the sawdust is quite good because it comes mainly from quality solid wood
  - They don’t have much scrap materials in general (cut outs)
    - Because it’s mainly handwork and not that much CNC work (in contrary to what we have seen in other makerspaces such as Fablabs)
    - And, the leftovers are directly used by people that are using the workshop.
- We met with OpenStructures and our mentor Guillem to discuss our project’s approach, short term and long term objectives.
- We oriented the two weeks towards finding a binder to reuse the sawdust and produce a biocomposite material with which we’d like to make OS_parts.

## Readjusting the challenge

![slide 17](HGHL_finalpresentation_17.jpg)
![slide 19](HGHL_finalpresentation_19.jpg)
![slide 20](HGHL_finalpresentation_20.jpg)

- New objective with OS
  - Explore new aesthetic of OS_parts - promoting new properties

- New objective with TMDC
  - Make use of the waste they produce

- Our objective
    - Create a recycling process that could be replicable and applied in TMDC and in other makerspaces

## Material exploration

![slide 21](HGHL_finalpresentation_21.jpg)

- We worked during those two weeks to experiment with material composites and asked people we knew to share their knowledge on some of the materials we wanted to try and use.
Starting with a small introduction of Precious Plastic given in Fab Lab
And with good advice and help, we made samples of sheets and blocks of sawdust mixed with HDPE.

- We also talked to Mohamad Atab from IaaC who is associated with urban biosystems, working on implementing natural-based solutions in cities through computational design and who already made a few experiments with mycelium
Following his advice, we made some tests in petri dishes (still growing) to see if we could make the process more simple for application within a makerspace. (sterilisation of sawdust, etc.)

- We met with Biobabes -a collective of feminist biodesigners, makers, and biohackers that aims to redesign our relation and interaction with the environment through materiality and fabrication- here in Barcelona to discuss different options for biomaterials (mushrooms versus scoby)

- Later we also arranged to meet with Anastasia Pistofidou, head of Fab Textiles Barcelona, to experiment with pine resin as a binder for our sawdust.
We tried various recipes and processes, but in general it was quite hard to get it exactly right, yet essential to make the biomaterial.

- We have gathered all of this knowledge to continue some experiments at home and create a range of tests using agar agar, alginate, cellulose, scoby, wax and white glue.

## Results

![slide 22](HGHL_finalpresentation_22.jpg)
![slide 23](HGHL_finalpresentation_23.jpg)
![slide 24](HGHL_finalpresentation_24.jpg)
![slide 25](HGHL_finalpresentation_25.jpg)

We wanted to find ways to reuse the waste material onsite and repurpose its unique properties.
Not looking at material for its structural strength

## Conclusion

![slide 26](HGHL_finalpresentation_26.jpg)

- Following these experiments, we concluded that we were more interested in processes which can be easily replicated and that could be realised within a makerspace environment.
  - Create a recycling process realised within a makerspace, build the tools  and make this raw presized material for specific use (such as acoustic panels, inside walls, wedges, etc.. ).

- The idea of a material that stays alive and keeps growing is a concept we find very interesting and we want to look into.
  - We want to bring to the fore the collaboration with microorganisms as essential because it is a natural process requiring low energy, naturally occurring in nature, and helping us be more conscious of our natural environment and its resources.
  - Working with a different time frame would allow us to reconsider the material itself - “precious sawdust” idea

- We’d like to focus on mycelium and try to grow it in sawdust, following the constraints of the grid to create a new material composite already based on the OS grid.
  - This would allow us to see how the living material grows and evolves with the grid, and find its unique properties for new purposes other than structural ones (not try to mimic a chipboard...).


## More to come

![slide 27](HGHL_finalpresentation_27.jpg)

This 3-week exchange gave us the possibility to initiate this project and, because we haven’t reached the goals we wrote down, we wish to continue it till there.
