---
title:  Introduction
tags: Wetlab-Amsterdam, biodesign, biocomposite, Fablab-Intern
---

Conor and I wanted to do experimentations with mycelium.

The mycelium is the vegetative part of a mushroom consisting of a mass of branched and filiform hyphae. Fungal colonies composed of mycelium are found in and on the soil and many other substrates. Through the mycelium, a mushroom absorbs nutrients from its environment. Mycelium is essential in ecosystems for its role in the decomposition of plant material. They contribute to the organic fraction of the soil.

It's a very rich material that shows a lot of interest because of the rapid way it develops by feeding on organic matter and due to the mass of branched hyphae that makes it compact.

From this knowledge and experimentation came the idea to grow some mycelium with straw to create a new composite material. Some way to assembly straws can be very strong just by itself. If we were to grow mycelium into those structures, the mycelium will became the «glue» to make this structure even stronger.

On that way, we can easily imagine blocks or sheets of this new material which could be used with Fablab’s machines like laser cutter or CNC milling machine in order to continue to prototype, to try, to fail, to learn and retry by using a more sustainable material that you can easily grow on site and easily compost the rest.

For now, the only stuff developed with mycelium are decorative things like lampshades or soft material to replace the foam for example but not yet in a strong version to build stuff.

Main advantages of this process:

-	It could be grow easily by yourself at home, in a workshop or in a non professional laboratory.
-	It’s a completely natural process with only two main components : straw and mycelium.  You just need a bit of water, coffee grounds or flour to help the mycelium to grow.
-	It’s growing faster and don’t need so much space.  
-	You can easily compost this material after use.
