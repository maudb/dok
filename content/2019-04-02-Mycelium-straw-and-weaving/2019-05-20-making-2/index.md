---
title:  Making - test 2
tags: Wetlab-Amsterdam, biodesign, biocomposite, Fablab-Intern
---

## Preparation:

For this second test, we wanted to explore the combination of mycelium and straw in a block form. The clear winner of the last test was the twisted and tied method so we decided to wove those "beehive style stick" together to create a more compact block where every parts held together. And we compared them against randomly arranged straw.

Samples with coffee and flour added were also prepared, these are reported to boost growth by supplying additional nutrients. We also aquired a new grey oyster grain spawn to run this new test.

We first prepared stems of rye straw of ~20cm (because of the size of our boxes and because it fits in the autoclave), we sterilized them by autoclaving, then we added sterilized coffee and flour to some straw package and finally inoculated each package of straw with the millet/mycelium combination.

→ See my previous [making post](2019-04-03-mycelium-straw-making.html) for more information about sterilization by autoclaving and inoculation.

NB : this time we let the autoclave plastic bag open during the process of sterilization to humidify the straw.

And then we prepared different kind of boxes :

- randomly arranged straw boxes (one with coffee, one with flour and one with nothing)
- twisted and tied straw sections woven together boxes (one with coffee, one with flour and one with nothing)
- only wood dust box to see if the longer fibres of straw add any strength at all. (one with coffee, one with flour and one with nothing)

![Millet grain spawn (inoculated with grey oyster mycelium](mycelium-straw-2-making-01.jpg)
![straw/mycelium mix with flour, nothing added and coffee respectively](mycelium-straw-2-making-02.jpg)
![wood dust tes](mycelium-straw-2-making-03.jpg)
![test with 6 twisted and tied straw sections woven together](mycelium-straw-2-making-04.jpg)
