---
title:  Making - test 3
tags: Wetlab-Amsterdam, biodesign, biocomposite, Fablab-Intern
---

Let's do a third test in a more compact environment and completely fill with straw.
The idea this time is to continue to try the twisted and woven composition of straw but putted in a plastic bag and not in a box anymore. Then, fill the bag with cut straw to avoid holes.

But we first cut pieces of 20cm of straw and sterilize them by autoclaving. We also put the rope we gonna use to twist the straw in the autoclave. When the straw cooled down to a room temperature, we inoculate some mycelium [oyster grain spawn on Millet](https://www.annforfungi.co.uk/shop/oyster-grain-spawn/) to the straw. We let the mycelium grow like that for few days.

→ See my previous [making post](2019-04-03-mycelium-straw-making.html) for more information about sterilization by autoclaving and inoculation.

![](mycelium-straw-3-making-01.jpg)

After few days, the mycelium grew a bit in the plates.

![](mycelium-straw-3-making-02.jpg)
![](mycelium-straw-3-making-03.jpg)

Then we decide to twist and wove the straw.

![](mycelium-straw-3-making-04.jpg)
![](mycelium-straw-3-making-05.jpg)

And we put them in a bag by filling holes with little pieces of straw we also sterilized and inoculated with mycelium in the same time. We made holes all around the bag with a knife to let air comes inside, it is necessary for the mycelium growth. And we roll parafilm around the bag to avoid contamination. Then we put them into the incubator (26°).

![](mycelium-straw-3-making-06.jpg)
![](mycelium-straw-3-making-07.jpg)
![](mycelium-straw-3-making-08.jpg)
