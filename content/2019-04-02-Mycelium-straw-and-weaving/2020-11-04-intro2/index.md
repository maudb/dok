---
title: Caña
tags: weaving, biodesign, biocomposite
---

# Resources

[NUPURI FILMS video](https://www.youtube.com/watch?v=z0IcPxCvIcA) in which the bamboo artist Jiro Yonezawa shows how to prepare bamboo for weaving.

[XiaoXi's Culinary Idyll videos](https://www.youtube.com/playlist?list=PLT7JTZMkE-QyEk8QlitkJ7e1o10oNxKW_) in which he shows how to make Bamboo weaving baskets from raw bamboo.

both are using a very interesting "split bamboo knife" (竹割包丁/竹割り包丁)

TMDC organised a workshop in Barcelona called [Casal d'Estiu - Construcción con caña](https://www.tmdc.es/blog/construccion-con-cana) in which Marta Arnal de [Voltes coop](https://voltes.coop/) shows how to weave caña which is a local "baboo"
