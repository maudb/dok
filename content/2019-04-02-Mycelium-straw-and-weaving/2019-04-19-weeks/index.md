---
title:  Evolution over the weeks - test 1
tags: Wetlab-Amsterdam, biodesign, biocomposite, Fablab-Intern
last_update: 2019-05-13
---

## Week 1

![Dish 1 : Random Straw](mycelium-straw-week1-01.jpg)
![Dish 2 : Layered Straw](mycelium-straw-week1-02.jpg)
![Dish 2 : Layered Straw (back)](mycelium-straw-week1-02b.jpg)
![Dish 3 : Woven Straw](mycelium-straw-week1-03.jpg)
![Dish 3 : Woven Straw (back)](mycelium-straw-week1-03b.jpg)
![Dish 4 : Straw Husk](mycelium-straw-week1-04.jpg)
![Dish 5 : Coconut Fibr](mycelium-straw-week1-05.jpg)
![Box 6 : Beehive Style](mycelium-straw-week1-06.jpg)

The mycelium we had in the lab was not a strong specimen and growth was slower than expected. The samples of the woven (Dish 3) and layered (Dish 2) tests showed the most growth.

Most of the mycelium we used to inoculate the twisted sample (Box 6 - beehive style) fell out while actually twisting and tying the small sample. Thus, there was a limited amount feeding on the straw itself.

The driest samples (Dishes 4 and 5) showed almost no growth.

## Week 2

![Dish 1 : Random Straw](mycelium-straw-week2-01.jpg)
![Dish 2 : Layered Straw](mycelium-straw-week2-02.jpg)
![Dish 2 : Layered Straw (back)](mycelium-straw-week2-02b.jpg)
![Dish 3 : Woven Straw](mycelium-straw-week2-03.jpg)
![Dish 3 : Woven Straw (back)](mycelium-straw-week2-03b.jpg)
![Dish 4 : Straw Husk](mycelium-straw-week2-04.jpg)
![Dish 5 : Coconut Fibre](mycelium-straw-week2-05.jpg)
![Box 6 : Beehive Style](mycelium-straw-week2-06.jpg)

We found some green in Dish 1, the dish is contaminated because there was an hole in the parafilm, we decide to remove this dish from the box.

Dishes 2, 3 and Box 6 look good, the mycelium is growing. And next time, we'll put the twisted straws (beehive style) in a best fit box to force the mycelium to grow on the straw and not around.

The mycelium in Dish 4 and Dish 5 is not growing, the straw is definitely too dry.

## Week 3

![Dish 2 : Layered Straw](mycelium-straw-week3-02.jpg)
![Dish 2 : Layered Straw (back)](mycelium-straw-week3-02b.jpg)
![Dish 3 : Woven Straw](mycelium-straw-week3-03.jpg)
![Dish 3 : Woven Straw (back)](mycelium-straw-week3-03b.jpg)
![Dish 4 : Straw Husk](mycelium-straw-week3-04.jpg)
![Dish 5 : Coconut Fibre](mycelium-straw-week3-05.jpg)
![Dish 5 : Coconut Fibre (back)](mycelium-straw-week3-05b.jpg)
![Box 6 : Beehive Style](mycelium-straw-week3-06.jpg)

Dishes 2, 3 and box 6 still growing well but after 3 weeks the mycelium growth is only what would be expected at 1 week.

Dishes 4 and 5 do still not grow, only in the back of the Dish 5 because we reused the petri dish where our mycelium mother grown and so now, the mycelium is growing into the coffee grounds that was left in the petri dish.

## Week 4

Laura, an other intern at Waag from Germany, is trying to make mycelium leather following [this method from the BioFarmForum](https://biofabforum.org/t/method-of-making-mycelium-leather/218).

Because she came back to Germany for one month, she asked us to give some food to her mycelium.

![](large:mycelium-straw-week4-00.jpg:flux)

We took this opportunity to put some food in our petri dishes aswell to boost the growth.

The nutrient-rich liquid is composed by :  

- Malt extract (3g)
- Yeast extract (3g)
- Peptone (5g)
- Glucose (10g)
- Distilled water (1000L)

and sterilized by autoclaving

![](mycelium-straw-week4-01.jpg)
![](mycelium-straw-week4-02.jpg)
![](mycelium-straw-week4-03.jpg)
![](mycelium-straw-week4-04.jpg)

The funny fact is, as you can see above, the mycelium is waterproof and the nutrient liquid doesn't go inside the composition. But we figure out this fact by putting the nutrient liquid where there is less mycelium. After this inoculation food giving process, we close the dishes and box with parafilm and reput them into the foam box in the boiler room, like before.  

![Dish 2 : Layered Straw](mycelium-straw-week4-12.jpg)
![Dish 3 : Woven Straw](mycelium-straw-week4-13.jpg)
![Dish 3 : Woven Straw (back)](mycelium-straw-week4-13b.jpg)
![Dish 4 : Straw Husk](mycelium-straw-week4-14.jpg)
![Dish 5 : Coconut Fibre](mycelium-straw-week4-15.jpg)
![Dish 5 : Coconut Fibre (back)](mycelium-straw-week4-15b.jpg)
![Box 6 : Beehive Style](mycelium-straw-week4-16.jpg)

## Week 6

![Dish 2 : Layered Straw](mycelium-straw-week6-02.jpg)
![Dish 2 : Layered Straw (back)](mycelium-straw-week6-02b.jpg)
![Dish 3 : Woven Straw](mycelium-straw-week6-03.jpg)
![Dish 3 : Woven Straw (back)](mycelium-straw-week6-03b.jpg)
![Dish 4 : Straw Husk](mycelium-straw-week6-04.jpg)
![Dish 4 : Straw Husk (back)](mycelium-straw-week6-04b.jpg)
![Dish 5 : Coconut Fibre](mycelium-straw-week6-05.jpg)
![Dish 5 : Coconut Fibre (back)](mycelium-straw-week6-05b.jpg)
![Box 6 : Behive Style](mycelium-straw-week6-06.jpg)

Dishes 2, 3 and Box 6 are continued to grow but we don't know if it's due to the food.
But the food helped Dish 4 and killed Dish 5. I think we put too much in Dish 5.

Anyway, because we read that 6 weeks is the longest time for this oyster mycelium growing before mushrooms appear, we decided it's time to stop the growing process and dry the mycelium and finally test the resistance of those compositions.

![](mycelium-straw-week6-07.jpg)
![](mycelium-straw-week6-08.jpg)

So we put the content of Dish 2, Dish 3, Dish 4 and Box 6 in the oven during 45 min at 200°C.

![After drying, everything looks a bit stronger](mycelium-straw-week6-09.jpg)
![But the different layers of straws don't stick together](mycelium-straw-week6-10.jpg)
![The dried layered straw is a failure, the mycelium didn't grow between the different layers and the composition is not strong enough to allow the mycelium to glue the different elements together. But you can see that it still worked a little, only it's very brittle](mycelium-straw-week6-16.jpg)
![The dried woven straw is not so bad but it breaks very easily aswell, maybe because it's only one layer and so it's very fragile](mycelium-straw-week6-12.jpg)
![dried woven straw](mycelium-straw-week6-14.jpg)
![dried woven straw](mycelium-straw-week6-15.jpg)
![dried woven straw](mycelium-straw-week6-20.jpg)
![The dried beehive style is the most promising!](mycelium-straw-week6-13.jpg)
![Certainly because the composition was stronger at the beginning](mycelium-straw-week6-17.jpg)
![But also because the mycelium grew inside the straw and glue everything together](mycelium-straw-week6-18.jpg)

The result of the Husk straw was so anecdotic to talk about it.

## What we learned in this first test

- We need a very strong composition of straw at the beginning to hope have a stronger composition with mycelium. In most of the tests the mycelium grew mostly on the surface of the straw, only adding superficial strength. Only the tiwsted and tied “ skep bee- hive” style test survived handling.
- Mycelium needs a humid environment to grow so it's better to wet the straw before doing the composition.
- try a different way to dry the result to keep it more flexible and less brittle. Maybe by following [this method from the BioFarmForum](https://biofabforum.org/t/method-of-making-mycelium-leather/218)
- time to try something bigger and thicker through the twisted test was by far the best, we will use this sample in a new run of tests with a stronger, faster growing mycelium specimen.
