---
title:  Making - test 1
tags: Wetlab-Amsterdam, biodesign, biocomposite, Fablab-Intern
---

## Preparation

For this fist test, we wanted to see how the mycelium will grow in different type of straw constructions. We prepared 4 petri dishes with rye straw in different forms. We also had some dried coconut fibre and so decided to prepare this for a test also.

![cutting rye straw to fit in petri dishes](mycelium-straw-prep01.jpg)
![prepare different petri dishes with different forms of straw](mycelium-straw-prep02.jpg)
![humidify some straw to wove it](mycelium-straw-prep03.jpg)
![wove the wet straw](mycelium-straw-prep04.jpg)
![](mycelium-straw-prep05.jpg)

### Dish 1: Random Straw

First we prepared a dish with scraps of some of the larger diameter sections of straw from our supply, these were placed randomly in the dish to act as a control to compare its strength to the woven and layered straw tests.

Straw diameter: 4.5-5.5mm

### Dish 2: Layered Straw

We then prepared a dish with lengths of straw laid side by side, alternating the direction of the rows by 90 degrees each time like the layers of wood in plywood. For this test we used the smaller diameter sections of straw to try and simulate a larger test by raising the straw sections per centimetre.

Straw diameter: 1.5-3mm

### Dish 3: Woven Straw

Next we prepared a dish with straw sections woven in an over-under pattern at 90 degrees to one another, again using the smaller diameter straw. We soaked the straw in warm water first as it needed to be flexible to stop it from breaking during weaving.

Straw diameter: 1.78-3.5mm

### Dish 4: Straw Husk

We used the smaller and more broken up straw scraps to fill another Petri Dish to see if the already broken straw would react differently with the mycelium than the more lengthy sections used in the other tests.

### Dish 5: Coconut Fibre

Finally, we had some  dry coconut fibre in the Fablab so we decided to test this as a curiosity. We filled the dish randomly with the coconut fibre as we did with the husk and the "random straw"

### Food Box: Beehive style a.k.a “Twisted and tied”

Finally we prepared a test using an ancient technique for making beehives where the straw is twisted tightly before being tied every few CM. Large diameter straw was used.

Straw diameter: 3-5mm

## Sterilization

Then, we sterilized everything with the autoclave method :

![](mycelium-straw-prep06.jpg)

1. Put all the petri dishes (closed with tape) and all the straw you want to sterilize in an autoclave plastic bag and close the bag with autoclave tape (black lines appears on the tape when the autoclave process is finished)
3. Put the bag in the autoclave and fill 1/3 of the autoclave with water
4. Close the autoclave (make sure it's perfectly closed), lock it and turn on the heat to the maximum
5. It takes normally 10-20 min for the red pressure valve goes up (it's like a little button located on the lid of the autoclave)
6. Then turn the heat to the middle point and wait 20 min more
7. Release the pressure by keeping the valve up with a tweezers and then open the autoclave
If there is black lines on the autoclave tape, the sterilization process is done !


## Inoculation

 The strain we used form the test was an oyster mushroom variety which had been grown from spores on agar in the Biolab at Waag. We used some previously inoculated used coffee grounds which had been growing for around 3 weeks, by which time the petri dish which it was in had been mostly filled with mycelium. We worked around a Bunsen burner in the lab to keep a sterile working environment while we inoculated the straw samples with the mycelium grown on the coffee grounds. We tried to spread the coffee grounds evenly through the straw samples, this was easier in the loosely packed "random" straw than with the tightly packed woven and layered samples. the most difficult to get an even distribution of the mycelium was the 'twisted and tied' sample. the pieces of straw were packed tightly together so getting the mycelium deep into the sample was difficult. If we were to do it again we would have tried inoculating the straw before twisting it together.

![](mycelium-straw-prep10.jpg:flux)
![](mycelium-straw-prep11.jpg)
![](mycelium-straw-prep12.jpg)

At the end, we close every dishes and the foodbox with parafilm and put everything in a foam box (those used by food deliverers to transport frozen products) and we put this box in the boiler room because it's always warm in there.

NB: To inoculate in a sterilized environment we use a gas cylinder and ethanol. We place the gas cylinder in the middle of the table, we light the fire and we spread ethanol all around the gas cylinder within a radius of 40cm. Ethanol evaporates and this creates a sterile area between the table and the fire where you can work.
