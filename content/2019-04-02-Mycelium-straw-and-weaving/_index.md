---
 title: Mycelium, straw and weaving
 tags: biodesign, biohacking
 featured: True
 featured_image: mycelium-straw-prep11.jpg
 excerpt: exploration of mycelium growth in different straw structures
---
