---
title:  Evolution over the weeks - test 2
tags: Wetlab-Amsterdam, biodesign, biocomposite, Fablab Intern
last_update: 2019-06-06
---

## Week 1

The level of growth obtained after 1 week with the millet spawn is much better than the previous test.

The woven tests were surrounded by a mix of straw scarps and grain spawn in an attempt to counteract the fact that much of the grain spawn fell out of the twisted samples during preparation, just like in the previous test.

We also made samples of “randomly” ordered straw with coffee, flour and nothing added for comparison to the woven tests. Unfortunately, the boxes with flour and coffee were contaminated with some bad green inside even though we close the boxes with parafilm around the lid and made some holes on the top of the lid closed with pieces of wadding to ensure air renewal while keeping a sterile environment.

![woven twisted and tied with coffee ground](mycelium-straw-2-week1-02.jpg)
![woven twisted and tied with flour](mycelium-straw-2-week1-01.jpg)
![woven twisted and tied with nothing](mycelium-straw-2-week1-04.jpg)
![random straw with nothing](mycelium-straw-2-week1-03.jpg)
![wood dust with nothing](mycelium-straw-2-week1-05.jpg)
![](mycelium-straw-2-week1-06.jpg)

## Week 2

We stopped the test after week 2.

The leaders in terms of overall mycelium growth were the samples to which wheatflour was added to the straw before inoculating. The woven samples felt stronger than the unordered ones upon initial examination, in support of the hypothesis that strength in the substrate material (twisting and then weaving) is passed on to the resulting mycelium composite. The extra sccraps of straw scattered to fill in the gaps in the woven straw samples seem vital to the end strength, long, woven fibres are not enough by themselves.

![woven twisted and tied with flour](mycelium-straw-2-week2-02.jpg:flux)
![woven twisted and tied with flour](mycelium-straw-2-week2-01.jpg)
![random straw with nothing](mycelium-straw-2-week2-04.jpg)
![random straw with nothing](mycelium-straw-2-week2-03.jpg)
![wood dust with nothing](mycelium-straw-2-week2-05.jpg)

All of the test samples were pressed with a book press and then left to grow for another 5 days, in line with the process described by Phillip Ross in his patent for making strong bricks of mycelium material. This pressing is said to add strength and facilitate faster growth.

![](mycelium-straw-2-week2-06.jpg)
![](mycelium-straw-2-week2-07.jpg)

 You can see a test piece using wood dust as a substrate as it appears after pressing. The wood dust test was done as a curiosity but showed decent strength. It could possibly be used to fill in the gaps around the woven straw style samples for more strength.

Next step is to dry them.

## Week 3

We dried it in the oven during 45 min at 200°C ([like last time](2019-05-27-mycelium-straw-w1.html#week-6).

![front](mycelium-straw-2-kill-01.jpg)
![back](mycelium-straw-2-kill-02.jpg)

The result is interesting because it worked like a glue to attached straw together and then it makes the structure stronger but the problem is still that the mycelium when it is dried is very crumbly and so when the structure of straw and mycelium is not strong enough or when there is more mycelium than straw, those parts break too easily.

I think we should try two things:

- grow mycelium and the woven twisted straw in a bag and fill all the holes with little pieces of mycelium, then we gonna have a very compact bag of straw in where mycelium can grow and glue it
- try to soak the composition in glycerine before to dry it, like the [mycelium leather process](https://biofabforum.org/t/method-of-making-mycelium-leather/218), to make the mycelium less crumbly and more stretchy.
