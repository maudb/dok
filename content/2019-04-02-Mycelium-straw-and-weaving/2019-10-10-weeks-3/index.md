---
title:  Evolution over the weeks - test 3
tags: Wetlab-Amsterdam, biodesign, biocomposite, Fablab-Intern
last_update: 2019-10-30
---

## Week 1

![](mycelium-straw-3-week1-01.jpg)
![](mycelium-straw-3-week1-02.jpg)
![](mycelium-straw-3-week1-03.jpg)

## Week 2

![](mycelium-straw-3-week2-01.jpg)
![](mycelium-straw-3-week2-02.jpg)
![](mycelium-straw-3-week2-03.jpg)

## Week 3

![](mycelium-straw-3-week3-01.jpg)
![](mycelium-straw-3-week3-02.jpg)
![](mycelium-straw-3-week3-03.jpg)

## Week 4

![tiny mushroom are coming on the right](mycelium-straw-3-week4-01.jpg)
![](mycelium-straw-3-week4-02.jpg)

I thought the mycelium grew well inside the structure but when I removed the plastic film, I saw that actually the mycelium grew only around the structure, between the straw and the plastic film and not inside the structure. It could be because I did the structure very compact then there is not enough air inside the structure for the mycelium growth.

![](mycelium-straw-3-week4-05.jpg)

So the idea to fill a bag with the structure of straw and plenty of cut straw to avoid holes doesn't work like I did. A better way to do it could be to had an intermediate step between the first inoculation and second
