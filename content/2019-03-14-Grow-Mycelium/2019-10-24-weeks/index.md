---
title:  Evolution over the weeks
tags: Wetlab-Amsterdam, biodesign, Fablab-Intern
last_update: 2019-10-29
---

## Week 2

![](mycelium-growth-test-week2-01.jpg)

## Week 3

![](mycelium-growth-test-week3-01.jpg)
