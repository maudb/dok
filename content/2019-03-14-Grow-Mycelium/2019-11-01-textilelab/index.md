---
title:  TextileLab contribution
tags: Wetlab-Amsterdam", TextileLab-Amsterdam, biodesign, Fablab-Intern
---

Find below my contribution to the TextileLab biomaterial library of Waag:

## Mycelium Leather

[→ get the .pdf file](file:veganleather-mycelium.pdf)

![](mycelium-textilelab01.jpg)


### Test 1

For steps 1-2-3, read my post [Grow Mycelium - making](grow-mycelium.html#grow-mycelium-making) to know how to inoculate some mycelium into the liquid nutrient medium.

![the result after 10 days of incubation (step 4)](mycelium-leather1-00.jpg)
![the result after 10 days of incubation (step 4)](mycelium-leather1-01.jpg)
![Harvest the mycelium sheet (step 5)](mycelium-leather1-03.jpg)
![Plasticise the mycelium and let it soak for several hours (step 6)](mycelium-leather1-02.jpg)
![the result of the dried mycelium sheet after several hours in the oven (step 7)](mycelium-leather1-04.jpg)
![the result of the dried mycelium sheet after several hours in the oven (step 7)](mycelium-leather1-05.jpg)


### Test 2

![The result after 10 days of incubation](mycelium-leather2-01.jpg)
![The result after 20 days of incubation. I wanted to wait a bit more and hoped to have it more dense but it did the opposite, it started to reduce. I guess not enough food anymore and so it slowly started to die](mycelium-leather2-02.jpg)
![Anyway, I still plasticised and dried the mycelium sheet and the result is still fine but very thin](mycelium-leather2-03.jpg)
![I also did a try with some mycelium inoculated in dog food because the medium and the mycelium were really compacted together and the result is interesting because it is thicker than the other tests](mycelium-leather2-04.jpg)

## Mycelium and Straw

[→ get the .pdf file](file:composite-mycelium-straw.pdf)

Read my project [Mycelium, straw and weaving](mycelium-straw-and-weaving.html) to see the process.

![](mycelium-textilelab02.jpg)
