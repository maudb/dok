---
title:  Body Incubator
last_update: 2021-02-12
tags: biodesign, biohack, mycelium
featured: True
featured_image: body-incub01.JPG
excerpt: taking care of your micro pet
---

Wear this necklace for 3 weeks and incubate edible mushrooms with the heat of your body. If you take well care of your micro pet, you may harvest some!!

![](body-incub03.JPG:flux)
![](body-incub04.JPG)
![](body-incub05.JPG:flux)
![](body-incub02.JPG)
![](body-incub01.JPG)

# The necklace

- I bought small petri dishes in [Pidiscat.cat](https://www.pidiscat.cat/es/484-p-09-vidrio-cientifico/206-capsulas-petri-vidrio/6700-capsula-petri-vidrio-con-tapa-medidas-15x60-mm.html?search_query=petri&results=12)
- I designed and 3D printed a petri dish holder
- I knotted a rope to go around the neck

![Using my Prusa MINI printer](body-incub07.JPG)
![I tried to be more efficient by printing 4 at the same time but I had some bed adhesion issue](body-incub06.JPG)


# Inoculation

I inoculated some grey oyster spawn in coffee ground.
[How to inoculate mycelium](grow-mycelium-making.html)

![](body-incub08.JPG)




# Friends

I offered a few necklaces to my friends for the experience and the results were fruitful.

## Incubation period

![Sally](small:sally-01.JPG)
![Me](small:me-01.JPG)
![Benny](small:benny-01.jpg)
![Parents](small:parents-01.jpeg)
![Melina](small:melina-01.jpeg)
![Jess](small:jess-01.JPG)
![Lilas](small:lil-01.jpeg)
![Me](small:body-incub09.JPG)
![Me](small:body-incub10.JPG)
![Benny](small:benny-02.JPG)
![Benny](small:benny-03.JPG)
![Vera](small:vera-01.JPG)
![Melina](small:melina-02d.JPG)

## Fruiting periode

![me](body-incub11.JPG)
![Benny](small:benny-04.JPG)
![Melina](small:melina-02.JPG)
![Lilas](small:lilas-04.jpeg)
![Vera](small:vera-02.jpeg)
![Melina](large:melina-14c.jpg)
![Parents (it's a joke!)](small:parents-03.JPG)
![Melina (it isn't a joke!)](small:melina-15.jpeg)
