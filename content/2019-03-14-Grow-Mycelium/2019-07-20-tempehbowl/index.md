---
title:  Tempeh bowl
tags: Wetlab-Amsterdam, biodesign, Fablab-Intern
last_update: 2020-09-17
---

## Tempeh

→[What is tempeh and how to make some](2020-05-03-tempeh.html)

→[Why create a composite material with Mycelium](2019-04-02-mycelium-straw-intro.html)

## First shape try

![](mycelium-vrac01.jpg:flux)
![](mycelium-vrac02.jpg:flux)

Before the fermentation process, I put the preparation in a bowl against the edges and I put a smaller bowl above. I wrapped it up with plastic film and made holes with a knife.

As you can see on the picture, the mycelium didn't grow in the bottom of the bowl because because it didn't have enough air.
