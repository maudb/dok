---
title:  Making
tags: Wetlab-Amsterdam, biodesign, Fablab-Intern
---

I wanted to do some growth test with mycelium so I inoculate 3 types of mycelium in 4 types of nutrient medium.

.               | Schyzophilum | Grey Oyster | Tempeh starter
----------------|--------------|-------------|----------------
Coffee Ground   |      x       |      x      |       x
Dog Food        |      x       |      x      |       x
Liquid Nutrient |      x       |      x      |       x
Agar Nutrient   |      x       |      x      |       x


## Make and sterilise the medium

### Coffee Grounds

Coffee grounds are sterilised by the process of making coffee (when the boiled water pass into the coffee) so I directly took some coffee ground after making me a coffee and I spread it in 3 sterile petri dishes. Then I let them cool down to room temperature.

### Dog Food

I crushed dried dog kibble and I spread them in 3 glass petri dishes that I reserved.

### Liquid Nutrient

I prepared a pot with:

- 250ml distilled water
- 0,75g of Malt extract
- 0,75g of Yeast extract
- 1,25g of Peptone
- 2,5g of Glucose

I mixed it and I reserved.

(The recipe come from the [biofabforum](https://biofabforum.org/t/method-of-making-mycelium-leather/218) )

### Agar Nutrient

I mixed 7g of agar powder with 250ml of distilled water in a pot and I reserved.

(The recipe was on the agar powder box)

## Autoclaving with pressure cooker

1. I first put pieces of autoclave tape in each petri dishes and pots and I put all of them in the pressure cooker.
2. I filled 1/3 of the pressure cooker with water
4. I closed the autoclave (make sure it's perfectly closed), lock it and turn on the heat to the maximum
5. It takes normally 10-20 min for the red pressure valve goes up (it's like a little button located on the lid of the autoclave)
6. Then I turned the heat to the middle point and wait 15-20 min more
7. I Released the pressure by keeping the valve up with a tweezers and then I opened the
If there is black lines on the autoclave tape, the sterilization process is done !

Then, I let everything cool down to room temperature before the inoculation.


## Inoculation

See below my inoculation set up:

![](mycelium-growth-test-making-01.jpg)

To create a sterilised area, I turn on the gaz burner and spread some ethanol around it. Ethanol evaporates and this creates a sterile area between the table and the fire inside where you can work.  

![](mycelium-growth-test-making-02.jpg)

I filled all the petri dishes with the different kind of medium.

![](mycelium-growth-test-making-03.jpg)

I first inoculated mycelium schizophilum in the 4 different kind of nutrients.

![](mycelium-growth-test-making-04.jpg)

I then inoculated mycelium grey oyster in the 4 different kind of nutrients.

![](mycelium-growth-test-making-05.jpg)

I finally inoculated tempeh starter in the 4 different kind of nutrients.

![](mycelium-growth-test-making-06.jpg)

And I let them incubate at 26°C, and wait and see.

![](mycelium-growth-test-making-07.jpg)
