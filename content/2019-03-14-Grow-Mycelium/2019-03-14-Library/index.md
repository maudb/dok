---
title: Library
tags: personal-notes, biodesign, biohack, mycelium, mushrooms
---

## Readings

- Radical Mycology A to Z, Peter McCoy and Natassja Noell
- Mycelium running - how mushrooms can help save the world, Paul Stamets
- Growing Gourmet and medicinal mushrooms, Paul Stamets
- Fungi in bioremediation, G.-M. Gadd
- Psilocybin - Magic mushroom grower's guide, O.T. OSS and O.N. OERIC
- The mushroom cultivator - a practical guide to growing mushroom at home, Paul Stamets and J.S. Chilton
- Mycoremediation - fungal bioremediation, Harbhajan Singh
- Mycorrhizal biotechnology, D. Thangadurai, C.A. Busso, M. Hijri
- Cordyceps Cultivation handbook, William Padilla-Brown
- Fungi Materials Manual by Elise Elsacker (researcher), Kristel Peters (shoe designer), Winnie Poncelet (bio-engineer) from BioFab Forum
- Mushroom cultivation manual for the small mushroom entrepreneur
- Mycelium Material Study, Studio MUR MUR Chicago
- Nature: Collaborations in Design, Cooper Hewitt Design Triennial co-organized with Cube design museum
- The Mushroom at the End of the World, Anna Lowenhaupt Tsing

## Videos/ Movies

- [Schimmel het plastic van de toekomst](https://www.vice.com/nl/article/pg338b/schimmel-het-plastic-van-de-toekomst)
- [Paul Stamets: Mushroom Magic](https://www.youtube.com/watch?v=mH88FBnMIX4&feature=youtu.be)
- [Paul Stamets: 6 ways mushrooms can save the world](https://www.youtube.com/watch?v=XI5frPV58tY)
- [Fantastic Fungi](https://fantasticfungi.com/)


## Visits

- [mediamatic](https://www.mediamatic.net/) NL, Amsterdam
- [Permafungi](https://www.permafungi.be/) BE, Brussels
- [Bluecity](https://www.bluecity.nl/) NL, Rotterdam
- [GreenLab](https://www.greenlab.org/blog) GB, London

## Workshops

- [Grow your own material workshop with Wouter Hassing at mediamatic](https://www.mediamatic.net/en/page/376728/growing-mushrooms)
- [MA-DE](http://www.materialdesigners.eu/)
- [BioHack Academy](https://biohackacademy.github.io/)

## References

- [Atelier Luma](https://atelier-luma.org/) FR, Arles
- [Eric Klarenbeek](http://www.ericklarenbeek.com/) NL
- [Maartje Dros](http://www.maartjedros.nl/) NL
- [Krown](https://www.krown.bio/) NL, Hedel
- [Julian Hespenheide](http://www.julian-h.de/work/biotic-explorers/ / https://hackaday.com/2019/06/03/artistic-attempt-to-send-digital-signals-via-fungus/) GE (Biotic Explorers Research Group)
- [Maurizio Montalti](https://www.corpuscoli.com) NL, Amsterdam
- [Ecovative Design](https://ecovativedesign.com/) Green Island, NY
- [The Growing Pavilion](https://companynewheroes.com/project/the-growing-pavilion/) NL
- [The Growing Pavilion Atlas](https://companynewheroes.com/wp-content/blogs.dir/18/files/2019/10/the-growing-pavilion-atlas.pdf?x54139)
- [Huis Veendam Bio-Laminates](https://www.huisveendam.com/) NL, Groningen
- [Local Forms](https://www.instagram.com/localforms/) A material research and adventure into mycelium
- [Mykor Design](https://www.instagram.com/valentinadipietrodesign/) UK
- [Tree-shaped structure shows how mushroom roots could be used to create buildings](https://www.dezeen.com/2017/09/04/mycotree-dirk-hebel-philippe-block-mushroom-mycelium-building-structure-seoul-biennale/)
- [the potential of mushrooms](https://www.dezeen.com/2018/09/25/state-of-the-worlds-fungi-report-mushrooms-eat-plastic-kew-gardens/)
- [Blast studio](https://www.instagram.com/blast.studio/) 3D printed mycelium in UK, London
- [Natura](https://www.naturadesign.org/) UK, "work with the ecosystem by redesigning current biosystems"
- [BioHm](https://www.biohm.co.uk/) UK "multi-award-winning research and development led, bio-manufacturing company. We allow nature to lead innovation, to revolutionise construction and create a healthier, more sustainable, built-environment"



## Suppliers and recipes

- [BioFab Forum](https://biofabforum.org/)
- [BioFabForum: Mycelium Manual](https://biofabforum.org/t/growing-materials-at-home-hard-mycelium-materials-manual/201)
- [Ann Miller's Speciality Mushrooms Ltd](https://www.annforfungi.co.uk/) (UK)
- [krown e-shop](https://www.grown.bio/shop/) (NL)
- [Mycophilia](http://mycophilia.nl/) For the love of fungi (NL)
- [Mycelia](https://www.mycelia.be/en) Mycelium for professional (BE)
- [CNC Exotic Mushrooms](https://www.cncexoticmushrooms.nl/) (NL)
- [Cultivar Seitas](cultivarsetas.es/) (ES)
- [Low-Tech Lab: mycelium's objects](https://wiki.lowtechlab.org/wiki/Objet_en_myc%C3%A9lium_de_champignon#)
- [Low-Tech Lab: Pleurotes culture](https://wiki.lowtechlab.org/wiki/Culture_de_pleurotes#)


## Contact

- Conor Croasdell (former intern at Waag) → see his project [a body heat incubator](https://www.dropbox.com/s/20g3jm8wa38iisw/Research%2C%20Process.pdf?dl=0)
- Candyce Dryburgh (former intern at Waag and founder of local forms)
- Inga Tilda (former intern at mediamatic)
- Yaël (researcher at TU Delft)
- Wasabii (researcher at TU Delft)
