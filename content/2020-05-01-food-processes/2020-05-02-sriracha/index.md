---
title:  Sriracha
tags: fermentation, recipe, macrobiotic
---

## What's Sriracha salsa?

Sriracha salsa is a type of fermented hot sauce originated from Thailand made from a paste of chili peppers, distilled vinegar, garlic, sugar, and salt.

![](pimientos.jpg)
![](sri.gif)

## How to make Sriracha salsa?

For 30cl of Sriracha Salsa

### Ingredients

- 600g of various peppers
- 500g red jalapenos
- 200g red Thai pepper
- 100g Scotch Bonnets
- 6 garlic cloves
- 45g of sugar
- 1 tablespoon of salt
- 12cl of rice vinegar
- 12cl of water

### Recipe

1. Wash one's hands. Wash the peppers and remove the tails.
2. Mix the peppers, garlic, sugar, salt, water. Until a smooth paste is obtained.
3. Put the dough in a clean jar, cover the jar with a piece of cloth and a string. Place the jar on a plate in case the mixture "rotes" and overflows.
3. Let the mixture ferment for 5 days at room temperature.
4. Re-mix the mixture by adding the vinegar. Mix well.
5. Filter the mixture (with the back of a spoon on a Chinese) to obtain a super smooth sauce without pulp. The pulp can be saved to make a chili oil.
6. Simmer the sauce over medium / low heat for 30 min, until the sauce thickens. If foam appears, gently remove it.
7. Remove the sriracha from the heat and let it cool to room temperature before bottling it and putting it in the fridge. She kept there for several months.

*Gefeliciteerd, je hebt je eigen sriracha gemaakt!*
