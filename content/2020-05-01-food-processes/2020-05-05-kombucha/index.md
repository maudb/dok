---
title: Kombucha Recipe
tags: fermentation, recipe, macrobiotic
---

## What's Kombucha?

Kombucha is a beverage made by fermenting sweet tea. The most intriguing in this process is that you will need a mother kombucha (aka a scoby) to brew your beverage.

## How to make Kombucha?

The process needs three steps:

## 1. Find a scoby

“The beverage name Kombucha tea has its origin in the Kombucha culture. The culture (or mother) is a symbiosis of bacteria and yeast. The microbial population is called Scoby and can grow through fermenting sweet black tea. A Scoby is a beautiful gelatinous blob which will give your Kombucha life. It contains all the yeast bacteria for the next generations. To make the Kombucha you will need a Scoby, but you will also need Kombucha to make more Scobys. Remember to always wash your hands when handling the Scoby to not risk to contaminate it.„

The most common practice is to share extra scoby with friends. Indeed, more you do kombucha, more your scoby grows. It becomes thicker and different layers appear. You can then separate it. From that moment, you can either keep it in a scoby hotel (a container with several scoby bathed in a preparation of kombucha), or give it to your friends to start their own production, or dry it to make [fake-leather](https://chrisbeckstrom.com/tutorials/making_kombucha_leather/).

![](kombucha-00.png:flux)
![](kombucha-01.png)

I received mine from the nice plant-based food shop where I used to go shopping. As you can see on the picture, it was initially a small round but from the moment I put it in my 5L jar, the scoby slowly started to expand. That is what scobies do, they first expand on the surface of the kombucha preparation, and then, they start to grow in layers. It is also why it is easy to make big sheets of kombucha scoby for fake-leather purposes.


## 2. First fermentation

When you found your (mother) scoby, you can brew your own kombucha. You will always need a mother batch with all the good nutrients and yeast to brew kombucha, it's the same for kefir making, sourdough making, or even gingerbeer making.  

### Ingredients

- 2L of (filtered) water
- 12g of black tea
- 3/4 Cup organic cane sugar
- Your mother kombucha aka scoby

### Materials

- Big glass jar
- Natural cloth and rubber band which fits over the jar.

### Process

1. Boil half of the water in a pot
2. When boiling add the tea and let boil for another 10 minutes
3. Remove the tea and poor it into the glass jar.
4. Add the another half water of room temperature to the glass jar
5. Add 3/4 cup organic cane sugar to the tea and stirr thoroughly until sugar is dissolved.
6. Let the tea-sugar mixture cool down to room temperature (This extremly important! If the tea is too hot it will kill all your bacteria)
7. After the tea mixture is cooled to room temperature add the scoby
8. Cover the jar with a cloth and tighten with rubber band
9. Put your Kombucha mixture in a dark corner of your room with good air circulation
10. Let the Kombucha work for the next 7 to 10 days (No peeking, no mixing, no touching!)

![](kombucha-02.png)
![](kombucha-03.png)


## 3. Second fermentation

This step can be skipped if you prefer unflavored kombucha. In both case, you have to collect the liquid (the fermented tea) and store it in flip top bottles.

![](kombucha-04.png)

### Ingredients

- Your fermented tea aka your kombucha drink
- Any fruit or ginger or herbs (feel free to experiment)

### Materials

- Flip top bottles

### Process

1. Collect your existing fermented Kombucha and divide it up equally in your bottles.
2. Keep 1 cup of the fermented Kombucha to store your scoby. The best is to let the scoby in the jar with the bottom of the fermented tea (and the small yeast particles that stick to it).
3. If desired add about 1/4 cup of fruit or flavor to the bottles.
4. Leave your bottles on the countertop and „burp“ them every day. Be sure to do that in your sink to prevent a mess. (Burping is opening up the bottles slightly so the excess carbon can be released).
5. After about 2-4 days the fermentation process is completed (depending on how carbonated you want it).
5. Keep the bottles in the fridge.

Kombucha tastes the best when its chilled! Make sure to drink it within a week (burp it every now and then) otherwise the alcohol level will rise.
