---
 title: Food Processes
 tags: recipe, fermentation, macrobiotic
 featured: True
 featured_image: food-cover.jpg
 excerpt: exploration of fermentation processes of food and beverages
---

# Introduction

Through my education and my professional practice, I always wanted to shape the future and promote the principles of a sustainable economy. When I understood that my alimentation and my food consumption are certainly the best way to act and to promote ecological and social changes, I decided to buy fair, organic and waste-free as most as I can. For the same reasons, I also turned to a vegan diet.

More recently, I began to explore the processes of fermenting food and beverages as I learned about the benefits of a macrobiotic diet with Valentina Nelissen at [Deshima](https://www.macrobiotics.nl/) in Amterdam (International Center of macrobiotic). I thrive on making tempeh, sourdough bread, kombucha, kefir and many other small fermented food productions. Dealing with living beings to process food has opened my interests to biology.

In the same optic, I am also playing the game to grow some of my vegetables and mushrooms. I took advantage of my time at Waag to build my own [bioponic system](growth-shelf.html), to learn about [mycelium and the kingdom of mushrooms](grow-mycelium.html), and I am learning urban farming and ecological interactions thanks to the [FabCity Hub](biolab-kitchen.html#the-fab-city-model-and-the-fab-city-hub-barcelona). I see sustainable food as a challenge and a power for the next future and as a target that materialises and influences ongoing deep changes in society and technology.

Moreover, I deeply think that going back on making, growing or processing your own food yourself aims to empower designers, among others, to assume a more proactive attitude, regarding food as a cultural vehicle of identity, innovation and social integration and open the door of biodesign (the practice of designing with biology).


# Ressources

[Deshima, Amsterdam](http://www.deshima.eu/)
