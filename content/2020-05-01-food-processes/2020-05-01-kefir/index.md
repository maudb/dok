---
title:  Water Kefir
tags: fermentation, recipe, macrobiotic
---

## What's Water Kefir?

Water kefir is an ancestral fermented-carbonated beverage that is produced using water kefir grains. Unlike regular kefir, which is made from milk, water kefir is made by combining sugar water with water kefir grains — a type of grain-like culture of bacteria and yeast.

The mixture is then typically fermented for 48 hours, producing a probiotic beverage rich in beneficial bacteria. It is refreshing, easy to enjoy and also packed with a lot of health benefits.

![](large:kefir-01.JPG)
![kefir grains mixed with brown cane sugar and water](kefir-00.jpg)


## How to make Water Kefir?

The process needs 2 fermentations.  

### First fermentation

#### Ingredients

- 4 big spoon of kefir grains
- 1L of water
- 2 big spoon of sugar (any type)
- 1 dried fig
- 1/2 lemon (bio!!) cut in slices or just the juice

#### Materials

- A glass jar
- a wooden spoon
- a plastic strainer
- a plastic funnel

DO NOT USE METAL

#### Process

1. Add the kefir grains, water and sugar to the jar
2. Mix to solve the sugar
3. Add lemon and fig
4. Place the jar outside of the fridge (room temperature) for 2-3 days


### Second fermentation

#### Material

- a glass bottle
- Flavors

#### Process

After 2-3 days of first fermentation,

1. Strain the fermented water into a new bottle and reserve the grains
2. Add taste makers (such as herbs, fruits, etc) into the bottle
3. Store the water kefir in the fridge for up to two weeks

You can already drink it right away ! (longer in the fridge = more flavour and more sour)


## What to do with the grains?

The kefir grains can be used (over and over) again for a new first fermentation.
If you don't want to do a new first fermentation right after, you can store the kefir grains into sugar water inside the fridge.

If you have too much kefir grains, you can give them to your friends, eat some as candy, or mix them in a smoothie.
You can also put them in your compost, it is good bacteries for the decomposition process.
