---
title:  Ginger Beer
tags: fermentation, recipe, macrobiotic
---

## How to make Ginger Beer?

The recipe I follow is [from FabCity Hub Barcelona](https://bcn.fab.city/)

![](gingerbeer-jess.jpg)

## Making a ginger bug

“You might think „what the hell is a ginger bug?“. Just like making Kombucha or a sourdough bread you need a „mother“ which contains all the good yeast for your fermentation. You will have to take good care of it and feed it regularly (once a week). And the day before you want to use it to make gingerbeer, you will also need to feed it. Then it is activated and full of energy for the fermentation. The day will come when your jar is full of gin- ger and very little fluid. You can either transfer your ginger bug to a bigger glass adding a cup of water to the jar. Or mix the ginger bug and then strain the ginger particles. Catch the fluid with your new jar, feeding it straight away with ginger and sugar!„

### Ingredients

- 1/2 Cups filtered water
- 1 Tablespoon organic ginger
- 1 Tablespoon organic sugar

### Materials

- 1 Glass jar with lid
- Natural cloth and rubber band which fits over the jar

### Process

1. Cut 1 tablespoon ginger in very small chunks (don‘t take of the skin, its nutrition for the yeast)
2. Combine the ginger and 1 tablespoon sugar in your glass jar, mix well !
3. Add the water to the ginger-sugar mixture and mix thoroughly.
4. Close the glass jar with the cloth and fix with the rubber band.
5. Let your ginger mixture rest for 24 hours. After 24 hours add 1 table- spoon sugar and 1 tablespoon ginger and mix well. Repeat this for 5 days.
6. On the sixth day you will see small bubbles on the top of the fluid. This means your fermentation went well. On the bottom of your jar you will see some white fluid, that is the yeast!
7. Now you only have to feed your ginger bug once a week. You can use the normal lid to close it.

## Making the drink

“Just as I said before you will need to feed your ginger bug the night before you wanna make a new batch of ginger beer. This will guarantee that your ginger bug is full with yeast and has enough energy for the fermentation process. You can flavour your gingerbeer with what ever you want. I usually make some hibiscus tea or lemonade and use the ginger bug to carbonate it. It tastes best fresh out of the fridge! Just try new things, its super fun! And no preservatives or anything else is in there, so its super healthy! After you have used the ginger bug for the fermentation, add the amount of water which you have taken out and feed again!„

### Ingredients

- 4 cups Filtered water Thumb size piece of Ginger 1/2 cup ginger bug
- 1/4 cup organic sugar

### Materials

- 1 Flip top bottle

### Process

1. Bring 3 cups of water to boil
2. Cut the ginger into slices and add them to the boiling water. Let sim- mer for about 15-20 minutes.
3. Stir your ginger bug to incorporate all the yeast into the fluid. Then strain about 1/2 cup from your ginger bug into a fresh cup, set aside.
4. After the ginger has cooked for 20 minutes. Strain the ginger pieces and add 1/4 cups of sugar.
5. Let your ginger water cool down. NEVER put your ginger bug into hot liquid. This will kill the microbes.
6. Add your ginger bug to the ginger water (and a splash of lemon if desired) and fill into the flip to bottle. Set aside and let ferment for about 3 days.
7. Burp the bottles every 2 days, other- wise they will explode because of the high carbonation. After the 3-4 days you can keep the gingerbeer in the fridge, use within a week.
