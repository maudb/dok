---
 title: Pleurotus Ostreatus
 tags: recipe, fermentation, macrobiotic, homefarming, mycelium
 last_update: 2021-02-14
 featured: True
 featured_image: prod-05.JPG
 excerpt: How to grow edible mushrooms at home
---

# Why Mushrooms are good?

Mushrooms are the fleshy fruiting body of a fungus. They grow above ground, soil or from a food source. Edible mushrooms are highly appreciated for them nutritional properties: they are good source of protein with an animo acid composition more similar to animal protein than plant protein, they are an important source of vitamin B and D, they are high fibre content and they also contain essential minerals for the proper functioning of our body such as selenium, phosphorus and potassium. Plus, it is a low calorie intake due to their high water content (80-90%). Traditionally, mushrooms have been used as a natural remedy for various diseases, especially in Asian folk medicine.

Many mushrooms are poisonous so gathering mushrooms in the wild is risky for the inexperienced and should only be undertaken by persons knowledgeable in mushroom identification. However, most mushrooms sold in supermarkets have been commercially grown on mushroom farms.


# How to grow mushrooms at home?

## Materials

- mushroom spawn: we ordered 5.5kg of Pleurotus Ostreatus (oyster) spawn from [cultivarseitas.es](https://cultivarsetas.es/11-comprar-semilla-de-seta-pleurotus-ostreatus.html) (MICELIOS FUNGISEM, S.A.)
- substrate: we filled a bag of straw from a local horse stable
- opaque bin bags
- alcohol 96°
- latex gloves
- pressure cooker (or a container to boil the straw)
- a dark space for the incubation
- a bright space for the production

## Step 1 - the sterilisation and the inoculation

1. We filled the pressure cooker with straw cut in small pieces and 1/4 of water (~1,5L in our case)
2. When the cooker was under pressure (when the valve is up) we waited 20 min more, then we turned off and let the pressure release. Note: it could also be done by immersing the straw in a bath of hot water (80°C) for two hours
3. We disinfected all the kitchen surface and put the gloves
4. Because we used new bin bags then we didn't need to sterilise them with alcohol because they are already sterile due to the manufacturing process (hot extruded plastic)
5. We opened the pressure cooker (the valve needs to be completly down!) and we moved wet straw in the bin bag by wringing it out as much as we could
6. we stirred the straw with our hands inside the bag to cool it down to room temperature
7. We weight the bag (= 1kg) and add 25% of mycelium spawn (= 250g)
8. We mix it well together with hands
9. We closed the bag and poked it with a tiny needle
10. We hanged it in a dark space with a room temperature of 15-18°C

![the mushroom spawn must be kept in the fridge to slow down their growth but it must be used within a month](prep-00.JPG)
![the straw need to be cut in small pieces for having a more dense substrate](prep-01.JPG)
![sterilising the straw with a pressure cooker and the work surfaces with 96° alcohol](prep-02.JPG)
![wringing the straw, moving it in the bin bag and let it cool down](prep-03.JPG)
![weight the bag of substrate and add 25% of mushrooms spawn](prep-04.JPG)
![mix well together](prep-05b.JPG)
![close the bag, poke it and hang it in a dark room of 15-18°C](gertrude.JPG)


## Step 2 - incubation

The mushrooms have inoculated once they have turned white. It could take 5-7 weeks in a dark room of 15-18°C or 3-5 weeks in a dark room of 21-24°C.

![](incub-00.JPG)

## Step 3 - Production and Harvest

1. Move the mushrooms into a bright and ventilated area and cut slits in the bags (around 4cm wide). The mushrooms will start to grow in the light and through the holes. Another option to be able to reuse the bags is to simply open the tops of the bags and allow the mushrooms to fruit from there rather than cutting holes in the bag.
2. When the mushroom starts fruiting, spray them daily with water with a spray bottle. If you do not have one you can sprinkle with water to ensure they do not dry out. If your mushrooms show a sign of infection (discolouration, the wrong kind of mould), cut it out with a knife.
3. Wait another 2 weeks or so and your first batch of mushrooms are ready to harvest.
4. To harvest mushrooms, twist the mushroom from the stem (it should make a crisp, cracking noise) or with a knife.
5. Waves of fruiting are obtained every 10 days. A wave is a cycle of production followed by few days without harvest. Usually, 2-3 waves are harvested but it is possible to obtain more.

![](prod-00.JPG)
![](prod-01.JPG)
![](prod-02.JPG)
![](prod-03.JPG)
![](prod-04.JPG)
![](harvest-01.JPG)

# Ressources

- [cultivarsetas.es/instrucciones.pdf](https://www.cultivarsetas.es/instrucciones/instrucciones_cultivo_setas.pdf)
- FabCity Hub Barcelona
- [grocycle.com](https://grocycle.com/)
- [healing-mushrooms.net](https://healing-mushrooms.net/Mushroom-Substrates)
- [Low-Tech Lab](https://wiki.lowtechlab.org/wiki/Culture_de_pleurotes/en)
- [Hyphal Fusion](https://hyphalfusion.network/)


# Notes

<!-- - the straw substrate seemed too humid. It should either be wrung out before putting them in the incubation bags (bin bag) or put the straw in a polypropylene bag (autoclave bag) during sterilisation with the pressure cooker.
- On Amazon: [PP bags special mushrooms](https://www.amazon.es/NICEXMAS-Cultivo-Polipropileno-Autoclavable-Sustrato/dp/B08JQNB25Q/ref=sr_1_3?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Bolsas+de+Cultivo+para+Setas+y+Hongos+autoclave&qid=1605278281&s=lawn-garden&sr=1-3) or [PP bags directly filled with straw](https://www.amazon.com/Pasteurized-Straw-Mushroom-Growing-Substrate/dp/B004L923LK)
- Could also mix the straw with coffee ground to have a substrate more balanced. Give a sterilised container to a local coffee shop and ask them to put the freshly brewed coffee grounds in it (to keep it sterilised)
- Some people mix ½ mushroom seeds and ½ straw substrate.

https://merveilles.town/@focus404/105361928932200689
https://learn.freshcap.com/growing/lime-pasteurization/

-->

## Issues

![First fruiting of the last bag.](prod-05.JPG)
![Most of them dried and didn't grow well.](prod-06.JPG)
![I still was able to had a good harvest.](harvest-02.JPG)
![But as you see here, even the new starts of fruiting dried directly despite regular watering.](prod-07.JPG)
![But the mycelium is not dead yet, there is new starts of fruiting behind the plastic. ](prod-09.JPG)
![Left: new starts behind the plastic. Right: they dry quickly once out of the plastic.](prod-08.JPG)
![The boiler room seems to have an environment that is too dry (although it is outside), so I moved them inside, to my room, and I will aerate daily and water regularly.](prod-10.JPG)
![We had some new fruiting that went to a mature shape](prod-12.JPG)
![But nothing comparable to the first fruiting](prod-11.JPG)
![](harvest-03.JPG)

## Logbook

**12/11**: We received the mycelium/mushrooms spawn order from cultivarseitas.es

### bags n°1

**13/11**: First batch of mycelium (5%) - 2 small bags (~1kg each)

**14/12**: Julia's one starts fruiting, we threw away the other one because the mycelium didn't spread well

### bags n°2

**25/11**: Second batch of mycelium (25%) - 2 small bags (~1kg each)

**14/12**: It starts fruiting! I moved them to a bright and ventilated area (= the boiler room which is an outside room with access via the terrace)

**21/12**: First harvest

**12/01**: Nothing happened since the first harvest, I moved them inside with the other bags.

**14/02**: Nothing happened to one of the two bags, the other got new tiny mushrooms but close to nothing.


### bag n°3

**01/12**: Third batch of mycelium with coffee grounds (25-50%) - 1 big bag (~2kg)

**18/12**: I cut slits because I saw little starts of fruiting and moved it to the bright and ventilated area (=boiler room)

**~23/12**: It started fruiting but I wasn't there to spray them daily with water so, when I came back on the 28/12, they had dried..

**29/12**: I continued to spray them daily

**12/01**: There is some new little starts of fruit behind the plastic (not into the slits), seems that the boiler room is too dry and maybe too hot even if it's outside. I moved them inside, into my room, which I hope will be enough ventilated for them.

**14/02** We've been able to harvest a few mushrooms here and there within this last month, but no longer beautiful productions like the first one. Most of the new little starts didn't grow to a mature shape despite a regular humidification with the spray bottle (~4x/day) and a renewal of air every morning for at least 30min-1h. Seemed that the environment was a bit better though because the little starts didn't dry directly like before but still, seemed that they couldn't grow further so I guess something was still wrong (the new environment wasn't good enough? or the deadline has been exceeded?). We stoped the production and mix the straw and mycelium with the soil we have for the plants.


### bags n°4

**10/12**: Last batch of mycelium (more than 50%) - 2 big bags (~2kg each)

**18/12**: I moved it to the bright and ventilated area (= boiler room) with the others bags but haven't cut slits yet

**21/12**: I cut slits even if I didn't see starts of fruiting because we're going away for 1 week

**28/12** Plenty of little mushrooms and plenty of new starts of fruit, I restarted to spray them daily as I didn't do it for a week because I wasn't there.

**05/01**: Harvest of the big mushrooms and let the littles to let them grow.

**12/01** The littles didn't grow and dried even if I spray them daily with water. There is definitely something wrong with the actual environment. But it seems that there is also new little fresh starts behind the plastic (not into the slits), so I moved all the bags inside.

**14/02** Same than bag n°3


## Still-life

Souvenir composition of this first mushroom farm experience:

![](Regina-01.JPG)
