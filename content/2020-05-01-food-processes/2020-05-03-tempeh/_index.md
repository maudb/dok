---
title:  Tempeh
tags: fermentation, recipe, macrobiotic, mycelium
---

## What's tempeh?

This is an originally Indonesian food product based on fermented soybeans. Molds form (white filaments, Brie-type cheese like) which transform the preparation into a kind of compact cake.

The taste evokes aromas of mushroom, nut and yeast. It is rich in proteins of vegetable origin and poor in lipids.

Usually, the tempeh is cut into slices and fried until the surface becomes crisp.

![tempeh made out of white soybeans](tempeh-00.jpg)
![tempeh made out of black beans](food-cover.jpg)

## How to make tempeh?

For 375g of tempeh

### Ingredients

- 250gr of skinless dried soybeans
- 1/4 tbsp of tempeh starter powder
- 1 tbsp of vinegar
- tap water

### Materials

- plastic film or a zip freezing bag
- incubator (or an oven, or a way to keep 27/31C in a closed space like a boiler room for instance)


### Process

1. Soak 250gr of skinless dried soybeans overnight
2. Rinse
3. Cook them during 25' with 1 tbsp of vinegar
4. Rinse
5. Let them cool down to room temp and dry them really well
6. In a bowl, add max 1/4 of tbsp of the tempeh starter powder and mix well with a spoon
7. Put the soybeans in a plastic film (or in a freezing bag), give a shape and makes holes all around to let the air in
8. Let ferment for 24-30 hrs at 27-31C in your incubator or in your oven with only the light on (NB: Nothing happen before 15 hrs)
9. Your tempeh is ready when it became a compact "cake" of white molds form/filaments
10. You can store it in the fridge to 1 week
11. To eat it, cut slices, fry them in a pan with olive oil till the color turn golden and the texture becomes crisp. Feel free to play with different flavors by adding spices during cooking.

[Watch it through my Instagram stories](https://www.instagram.com/stories/highlights/17864877952833548/)
