---
title:  Vegetable Broth
tags: food waste, recipe
---

## How to make your own vegetable broth out of your vegetables waste?

Easy Peasy!

1. Each time you cook, collect your vegetables waste in a container in the fridge (such as onion skin, garlic skin, carrot head and everything you don't eat from your vegetables)
2. After one week, put everything in a pan and add water until submerged all the content
3. Cook for one hour
4. Filter and keep only the liquid in a container, the rest can be composted
5. You can use it straight away to cook, or you can store them in the freezer by making broth cubes using an ice cube tray


![](broth_01.jpg)
![](broth_02.jpg)
![](broth_03.jpg)
![](broth_04.jpg)
