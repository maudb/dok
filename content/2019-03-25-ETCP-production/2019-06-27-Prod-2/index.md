---
title:  Fablab Production test-2
tags: furniture, modular-system, CNC-milling, Fablab-Amsterdam
---

I thought I had a second perfect plan to make the pieces of my modular furniture system with the CNC milling machine. It should really work according to me but my first try failed..

The main differences with the previous try are I only used 2D jobs, which is faster, and a sacrificed sheet as a completely flat and aligned support.

NB : I finally found the wood's stick with the right dimensions on OPITEC website:

- [Round parts (vertical parts) : ⌀20mm](https://nl.opitec.com/opitec-web/articleNumber/683500/zz/cID/c3I6cm9uZCBob3V0IDU=/p/5)
- [Half round parts (horizontal parts) : ⌀40mm](https://nl.opitec.com/opitec-web/articleNumber/685005/zz/cID/c3I6cm9uZCBob3V0IDU=/p/6) (This is a full round stick of wood, then I'll need an extra step to cut this stick in two parts)


## Preparing a support as a guide sheet

This step is made to be sure that the wood sticks will be completely flat and aligned with the CNC milling machine.

I needed a panel of wood as a sacrificed sheet, I used a MDF panel of 600x1220mm.

I first did a facing of the panel of MDF to make the support completely leveled withs the CNC milling machine. I used a big mill especially used for facing and I made a square on my panel by removing ~3mm.

Second, I drew straight lines of 1000mm by removing 3mm with a mill of 6mm (flat end 2 flutes) as a guide to put the wood sticks and be sure the wood sticks will be completely parallel with the CNC milling machine.

![](ETCP-prod2-01.jpg)

Then, the idea is to start with the wood stick of ⌀40mm with those 3 steps :

1. Make the notches : two holes of ⌀20mm at 40mm of the extremities.
It will be the holes to insert the vertical parts.
2. Cut the round stick in two half parts.
3. Turn the half parts in 45° and make the slots inside the notches.
The slots will receive the rings to lock the system.  

![](ETCP-prod2-00.jpg)

I first drew each steps in a .dwg files, then I configured the different steps in vCarve. It's important to work like that because on that way, every jobs (or every layers) are superposed and aligned.

You can have a check on my [.dwg file](https://www.dropbox.com/s/zvkf16x0gtph86e/etcp_CNC%20prod%20test%202.dwg?dl=0) and my [.crv file](https://www.dropbox.com/s/nry3n7w830b8gqm/etcp_CNC%20prod%20test%202.crv?dl=0).


## Make the half round parts (horizontal parts)

I put the ⌀40mm wood stick on the line I made before on the sacrificed sheet of MDF. Then I knew the wood stick is completely leveled and aligned with the CNC milling machine.

I screwed the extremities of the wood stick to the sacrifice sheet to attach it. So I sacrificed the ends of my wood stick (see in yellow) but with that solution, I didn't need to make extra clamp.

I also attached some pieces of wood on each sides of the wood stick to avoid vibration during the jobs.

Then I was ready to start the first job :  

### Make the notches

I used those setting :

PS : make sure your mill is long enough to go trough the ⌀40mm wood stick.

Tool            | `6mm flat end 2 flutes mill`
----------------|-------------------------
Pass Depth      | `2.0 mm`
Stepover        | `2.0 mm - (40%)`
Spindle Speed   | `1800 r.p.m`
Feed Rate       | `30.0 mm/sec`
Plunge Rate     | `20,0 mm/sec`

I first made two pockets of ⌀20mm at 40mm of the extremities of the final dimension of my wood. (Remember, I sacrificed the ends of the wood stick to put screws on it, to attach it (see in yellow). Then I needed to be extremely careful and keep a safe distance to run my jobs)

![](ETCP-prod2-02.jpg)

The pockets are exactly in the middle of the width of the wood stick thanks to the line I made before on the sacrificed sheet of MDF and to this method to work with superposed layers to generate the jobs.

![](ETCP-prod2-03.jpg)

But as you can see on this picture I had a problem with the next step, the cutting step. The mill touched the screw because I didn't keep a safe distance between the screw and my job.... it was very dangerous because then the little hot metal pieces of the screw went directly on the dust extraction with all the wood dust collected ! Luckily, there was no fire but I had to wait the day after to continue my work. (and more over, I broke the mill)


### Cut the round stick in two equal parts

I re did my job of the day before by keeping a safe distance from the screws. It worked but it was not in the middle of the stick.

![](ETCP-prod2-04.jpg)

I figured out the problem : I didn't select the good setting when I made my g-code in vCarve. I had to go on the line and not outside.

![](ETCP-prod2-05.jpg)
![](ETCP-prod2-06.jpg)


### Make the slots inside the notches

For this job, I had to change the mill for a smaller one to fit with the rings, and I used those settings :

Tool            | `3mm flat end 2 flutes mill`
----------------|-------------------------
Pass Depth      | `2.0 mm`
Stepover        | `1.2 mm - (40%)`
Spindle Speed   | `1400 r.p.m`
Feed Rate       | `40.0 mm/sec`
Plunge Rate     | `20,0 mm/sec`

![](ETCP-prod2-07.jpg)

I had the same problem with the slots than before with the cutting part, I was not on the line but outside. And I don't know why, but the second holes seems to not be in the center.

![](ETCP-prod2-08.jpg)

I wanted to run for a second time those three last steps to correct my mistakes.
I started again with the ⌀20mm notches but I used an other line on the sacrificed sheet to test.
Then, the notches was really not in the center of the stick.. I guess is again the same mistakes with the line setting. Then I decided it was too much for this test and I gave up for a bit..

![](ETCP-prod2-09.jpg)
