---
 title: ETC Production
 tags: furniture, modular-system, Fablab-Amsterdam
 featured: True
 featured_image: ETCP-prod3-13.jpg
 excerpt: exploration of ways of making ETCP
---

As I want to keep this modular furniture proposal open-source and reapplicable by everyone, I am working on the instructions by looking for a way of production that can be done by most people.

I took advantage of my time at Waag Fablab Amsterdam to explore the possibilities of merging this project to digital fabrication.

The idea is also to produce more parts to explore the limits of the project and make it evolve into a more complete system.
