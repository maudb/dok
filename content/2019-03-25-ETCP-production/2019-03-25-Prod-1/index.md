---
title:  Fablab Production test-1
tags: furniture, modular-system, CNC-milling, Fablab-Amsterdam
---

In 2014, I made the parts of my system with a dremel and other basic tools (drill, screwdriver, sander, etc). It was not perfectly precise but it was working well enough for a prototype.

Today, I want to see if it's easier and more precise to make it with the CNC milling machine because it's here in the fablab and on that way my project could be done by everyone who has access to a CNC milling machine.

## First trouble

I still haven't found yet in Amsterdam the right wood pieces with the exactly right measurements to fit with the [OS grid](https://openstructures.net/parts).

- Round part (vertical part) : ⌀20mm
- Half round part (horizontal part) : 40mm

So I came to the wood shop and bought pieces a bit more bigger to do first experiments.

- Round part (vertical part) : ⌀22mm
- Half round part (horizontal part) : 43mm

I decided to first try to adjust them with the CNC milling machine in order to let them fit with the OS Grid. It was really not easy because we speak about 2mm and it's very difficult to be so much precise by modifying existing pieces with the very big CNC milling machine we have here at the Fablab.

## Make a Clamp  

I first designed clamp to fix the pieces to the bed of the CNC milling machine.

![](ETCP-prod1-01.jpg)
![](ETCP-prod1-02.jpg:flux)

Here are the settings I used in vCarve to generate de g-code:

Tool            | `5mm flat end 2 flutes mill`
----------------|-------------------------
Pass Depth      | `2.0 mm`
Stepover        | `2.0 mm - (40%)`
Spindle Speed   | `1800 r.p.m`
Feed Rate       | `60.0 mm/sec`
Plunge Rate     | `20,0 mm/sec`                       

I used the same setting for the Roughting Toopath (Along Y) and the finishing Finishing Toopath (Along X).  


## Adjust pieces

I used the same settings as before to adjust the vertical round part and the horizontal half round part.

The first try with the ⌀22mm vertical round part was a fail, the diameter of the piece is now bigger on the top and smaller on the end and the only reason I see is because it was not completely flat. I also had a problem with my settings, the mill didn't go on both sides of the round part. But I figured out this problem, I have to add a margin around the model and then the machine accept to go on the sides.

The second try with the horizontal half round piece worked well, but working with the CNC milling machine to do this is too long... 50 minutes to remove 3 mm thick by 800 mm long! And I have to do this two times because there is two parts for this component. I certainly could have saved time by doing only the Finishing toolpath and skipping the Roughing Toolpath.

![](ETCP-prod1-03.jpg)


## Make the notches and slots

I started with the half horizontal round part. The first step was to install the set up with the clamps, and make sure that it was as flat as possible and as parallel with the machine as possible.

![](ETCP-prod1-04.jpg)
![](ETCP-prod1-05.jpg)

Again 50 minutes to make two notches... I have the feeling that the g-code could be smarter and do this faster by saving some useless movements. But how?

- Use faster settings? But it is not really allowed here because everybody is scared about this big CNC milling machine and I have no experience to try new settings.
- Try to make a smarter g-code ? I should try to use Fusion360, because it's a CAD (computer-aided designing) and CAM (computer-aided manufacturing) software. So you can set a lot of parameters and they have good previews.
