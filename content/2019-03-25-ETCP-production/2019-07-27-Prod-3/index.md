---
title:  Woodworking Production
tags: furniture, modular-system, woodworking
---

I finally decide to go on a woodworking space to work with more traditional machines like column drilling machine, format saw and other circular saw on table.

I went to a place in Amsterdam called [Openbare WerkPlaats](https://openbarewerkplaats.nl/home-en/) in order to rent workbench, tools and machinery for 2 days.

## Horizontal parts

I first had to make a guide at Fablab with the CNC milling machine to make possible the modifications of the rounded sticks of wood.

![](ETCP-prod3-02.jpg)

### Make a guide

The guide is for the ⌀40mm rounded sticks of wood in order to keep it in the same position for all the further steps. I bought a lumber cut in two parts in the wood shop, and I did the rounded shape with the CNC milling machine.

You can download the [.stl file](https://www.dropbox.com/s/mc186mujprsxgzc/OPW%20prod_guide%20.stl?dl=0) of the half part of my guide and make it two times with the CNC.

Here are the settings I used in vCarve:

Tool            | `5mm flat end 2 flutes mill`
----------------|-------------------------
Pass Depth      | `2.0 mm`
Stepover        | `2.0 mm - (40%)`
Spindle Speed   | `1800 r.p.m`
Feed Rate       | `60.0 mm/sec`
Plunge Rate     | `20,0 mm/sec`

I used the same setting for the Roughting Toopath (Along Y) and the finishing Finishing Toopath (Along X) and it took around 3 hours per piece(!!)

![](ETCP-prod3-11.jpg)
![](ETCP-prod3-12.jpg)


### Make the Notches

Once arrived at OWP, I put the stick of wood in the guide, close the guide with wood clamps and use the column drilling machine to make two notches of ⌀20mm at 40mm of the extremities.

(NB : ⌀20mm is the size of the vertical parts)

![](ETCP-prod3-07.jpg)


### Cut the stick in two equal parts

I kept the stick of wood in the guide and used the circular saw on a table to cut the rounded stick in two equal parts. The saw had a tickness of 4mm.

![](ETCP-prod3-08.jpg)


### Make the Slots

Then, I opened the guide, took the two half rounded sticks of wood and made slots in the middle of the notches at the two extremities of the half wood stick with the format saw. The slots have to be at 2mm depth at the deeper point inside the notches. Because we lost 4mm by cutting the piece in two, the deeper point inside the notches is at -8mm. Then we have to make the slots at 10mm depth.

![](ETCP-prod3-09.jpg)
![](ETCP-prod3-10.jpg)


## Vertical parts

I just use the format saw to make slots all around the rounded wood stick each 12 cm with a circular saw of 3mm and made slots of 2mm depth.

![](ETCP-prod3-01.jpg)
![](ETCP-prod3-03.jpg)
![](large:ETCP-prod3-05.jpg:flux)


## Result

![](ETCP-prod3-13.jpg)

PS : I finally decided to add two little holes on the two sides of the notches on each extremities of the half rounded horizontal parts in order to have the possibilities to add extra pieces and also to use bolds and nuts as a possibility to close the system.

![Here is the home-made column drilling machine we have in the Fablab](ETCP-prod3-14.jpg)
