---
title:  Hardware
tags: modular-system, OpenStructures, "bioponic-system, 3D-printing, Fablab-Amsterdam, Fablab-Intern
---

## Make a planter shelf

My idea was to buy planter pots and find a way to hang them to my [ETCP system](ETCP-ETCP-intro.html). I choose those [Elho Green basics balkonbak L 80 H 14 cm mild terra](Elho Green basics balkonbak L 80 H 14 cm mild terra) (two pieces of them to have two floors of plants) and an [Elho Green basics balkonbak L 40 H 14 cm groen](https://www.intratuin.nl/elho-green-basics-balkonbak-l-40-h-14-cm-groen.html) as water tank.

I designed and 3D printed 8 small red pieces which come to slip into the slot of the ETCP horizontal parts. These red pieces themselves have a slot to receive the planters.

![](growing-shelf-hardware-02.jpg)
![](growing-shelf-hardware-03.jpg)
![](growing-shelf-hardware-04.jpg:flux)


I was then able to reuse 4 ETCP horizontal parts and 4 ETCP vertical parts from the [chair](ETCP-ETCP-chair.html) I made before to make this new growing shelf.

To complete the structure of the shelf I used a leftover of a ETCP vertical parts ∅20mm that I slipped into a [OS parts(P.1097)](https://openstructures.family/parts/p1097) which I 3D printed at the lab. Actually, I modified a bit the pieces to make it fit better with my system. Feel free to ask me the 3D model if you need.

Then, to solve the triangulation problems, I stuck in the ends of the horizontal parts two metal rods that I crossed.

![](growing-shelf-hardware-01.jpg)

## The Ebb&Flow system and the Bell Siphon

As you can see in the image above, I also chose to 3D print a [bell siphon](https://worldwaterreserve.com/aquaponics/bell-siphon-for-aquaponics/) as I read is a good element for an Ebb&Flow bioponic system.

### What is an Edd&Flow System?

Ebb and Flow is well known in hydroponics (or bio-) because of its simplicity, reliability of operation and low initial investment cost. Pots are filled with an inert medium (like clay balls) which does not function like soil or contribute nutrition to the plants but which anchors the roots and functions as a temporary reserve of water and solvent mineral nutrients. The hydroponic solution alternately floods the system and is allowed to ebb away.

Because the fundamental principle of hydroponics (or bio-) relies on fertilized and aerated water which provides both nutrition and oxygen to a plant's root zone. E&F utilizes the fact that the solution is not left in constant contact with the roots of plants, to avoid the need for oxygenating of the solution.

Read more about on [wikipedia](https://en.wikipedia.org/wiki/Ebb_and_flow)

### What is an Bell Siphon?

Then a bell siphon comes in and Ebb and Flow system to regulate the amount of water. It keeps the solution to a certain level and then slowly allow the solution to ebb away which permit to the solution to give enough time to absorb the nutrient without the need to use the pump in a reverse way.

To design my one, I first watched videos to really understand how it works and I got inspired by a FabAcademy19 Student from Szoil Lab in China: [Haiyan-Su](https://fabacademy.org/2019/labs/szoil/students/haiyan-su/projects/final-project/) and it is composed by :

- The **funnel** which is the inner tube that comes through the planter pot. (To do so I'll then have to make a hole in my planter pot and find a way to ensure the watertightness, I chose to use a ring seal)
- The **bell for vacuum** which is the middle part. It needs holes on the bottom and I decided to give him a transparent lid to be able to see what is going inside. This lid also needs a hole in the middle to receive a tube, I used a transparent flexible PVC tube. This tube is placed between the middle part and the outer part waiting that the cap reach it. As a cap, I laser cut at the same time as the lid a circle that has the right dimensions to fit between the middle part and the outer part.
- The **media guard** which is the outer part that acts to keep rocks and other off the siphon pipe while allowing water to easily flow in and through the stand pipe. It has a base which ensures his stability as soon as the clay balls are sitting on it.

![6h35 of print](bell-siphon-01.jpg)
![the funnel with the ring seal in place](bell-siphon-02.jpg)
![](bell-siphon-03.jpg)
