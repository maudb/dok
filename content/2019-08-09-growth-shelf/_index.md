---
 title: Growth Shelf
 tags: bioponic-system, electronics, Fablab-Amsterdam, Fablab-Intern
 featured: True
 featured_image: growing-shelf-hardware-00.jpg
 excerpt: a bioponic shelf to grow plants and to learn electronics and programming
---
