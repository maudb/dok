---
title:  Begin with Electronics
tags: personal-notes, electronics, programming
---

Here is some links I followed to introduce me to electronics:

### Sparkfun tutorials Links:

- [voltage, current and resistance](https://learn.sparkfun.com/tutorials/voltage-current-resistance-and-ohms-law)
- [how to use a multimeter](https://learn.sparkfun.com/tutorials/how-to-use-a-multimeter)
- [resistors](https://learn.sparkfun.com/tutorials/resistors)
- [leds](https://learn.sparkfun.com/tutorials/light-emitting-diodes-leds/all)
- [analog VS digital](https://learn.sparkfun.com/tutorials/analog-vs-digital/all)
- [how to use a breadbord](https://learn.sparkfun.com/tutorials/how-to-use-a-breadboard/all)


### Arduino Code tutorial:

- [Programming Electronics Academy youtube channel](https://www.youtube.com/watch?v=09zfRaLEasY&list=PLZfay8jtbyJt6gkkOgeeapCS_UrsgfuJA&index=1)


### FabAcademy local class:

- [Emma's slide](https://www.dropbox.com/s/wkw48mw5mbku96x/fabricademy_2019.pdf?dl=0)


### Other links
- [Instructables Electronics Class](https://www.instructables.com/class/Electronics-Class/)
- [Instructables Solar Class](https://www.instructables.com/lesson/Solar-USB-Charger-2/)
