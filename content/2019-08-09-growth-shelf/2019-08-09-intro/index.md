---
title:  Introduction
tags: bioponic-system, electronics, Fablab-Amsterdam, Fablab-Intern
last_update: 2020-09-15
---


I now wanted to go a step further with my modular furniture system by experimenting with electronics.

My idea is to build a shelf with the same components I used to build the [chair](ETCP-ETCP-chair.html), and use this shelf to grow herbs and plants with a bioponic circular watering system. This bioponic circular watering system will be fed by compost juice, controlled by arduino and powered by solar panel.

![](ETCP-growingshelf-01.JPG)


In order to begin, I have to divide the work:

1. Make a "bokashi" to provide compost juice in order to feed the circular watering system.
2. Make the shelf in order to host a bioponics system.
3. Make an electronic system composed by a waterpump, a solar panel and a controller

PS: The final idea is to have a complete system like this one below.

![](ETCP-growingshelf-00.JPG)


## Why choose a bioponic system to grow plants?

### But first, what's bioponics?

Bioponics provides an organic alternative to normal hydroponic growing that uses synthetic fertilizers. Hydroponics is a subset of hydroculture, which is a method of growing plants without soil by instead using mineral nutrient solutions in a water solvent. Terrestrial plants may be grown with only their roots exposed to the nutritious liquid, or the roots may be physically supported by an inert medium such as perlite, gravel. Despite inert media, roots can cause changes of the rhizosphere pH and root exudates can impact the rhizosphere biology. The nutrients used in hydroponic systems can come from an array of different sources, including fish excrement, duck manure, purchased chemical fertilisers, or artificial nutrient solutions.


### advantages

- it's a good alternative if you don't have access to the soil.
- it's a save of water as soon as you work with a closed system.
- you constantly provide all the nutrients that plants need to grow into the water, so they can grow faster and stronger, and it's completely natural as soon as you put only compost juice.
- certainly a lot of other things I don't know


## Useful links

To work on my own system, I got inspired by those projects below:

- [hydroponics](https://wiki.lowtechlab.org/wiki/Hydroponie/en) from lowtechlab.org
- [miniponic](file:mini_manual_engels-54113751.pdf) by mediamatic.net
- Indoor growing guides by IKEA Poland in the context of [Home for tomorrow](https://homeoftomorrow.online/)
    - [aeroponic farm](file:aeroponic-farm.pdf)
    - [aquaponic farm](file:aquaponic-farm.pdf)
    - [spirulina farm](file:spirulina-farm.pdf)
    - [micro garden](file:microgarden.pdf)
