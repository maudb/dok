---
title:  Make a Bokashi
tags: Fablab-Amsterdam, food-waste, lasercutting
---

## What's a Bokashi compost

Ideal as an indoor composting system, the bokashi compost works with the help of Efficient Micro-organisms (EM). These are bacteria that break down matter without the help of vermicomposters. The advantage is that you no longer have to deal with these worms when you are out for long time.

The EM are the second strata of the decomposition process. Imagine, in a forest, the worms will first decompose the material broadly, then the bacteria take over. This is why the bokashi works hermetically because naturally, these bacteria are found very deep in the earth and works with very little oxygen. In the same logic, compost must be compacted with each refill.

We can put all the food waste there, but we have to chop them beforehand (the work initially done by the worms).

The input matter is actually fermented by those specialist bacteria, not decomposed, it's why this system gives you more juice than soil. This juice is a natural fermenter for plants which must be diluted with 99% water.

There is no smell because is hermetic, and it should take about 3 weeks to decompose. You can buy EM online or in an organic shop. It exists in powder or liquid.

The ideal is to have a table compost in which you put the chopped food waste and once it's full, you feed the bokashi with food waste and with the EM (just a little bit is needed, refer to the instructions on the packaging) and then you tamp and you close.


## How to make your own Bokashi

### Links

- [bokashi living](https://bokashiliving.com/) to understand more about
- [lowtechlab bokashi tuto](https://wiki.lowtechlab.org/wiki/Compost_Bokashi_de_cuisine/en) to make your own with food buckets
- [Bokashi Organko 2](https://www.skaza.com/bokashi-organko-2-1) get inspired by a the reddot design award winner 2019 to create my own design.

### How I made

1. I bought an outer flowerpot and an inner flowerpot with holes in the bottom that fit in.
2. For the lid, I lasercut in 4mm plywood an inner circle which fits exactly at the top of the flowerpot (this must be airtight) and an outer circle a little larger to handle it. I glued the small part in the center of the other with wood glue. If the inner circle doesn't fit perfectly, you can add a hermetic seal.
3. I bought a tiny tap and a nut and I made a hole in the bottom of the outer flowerpot.
4. I made a base in plywood 4mm with the lasercutter in order to be able to put a glass under the tap.

![](Bokashi-01.jpg)
![](Bokashi-04.jpg)
![](Bokashi-02.jpg)
![](Bokashi-03.jpg)
![](Bokashi-05.jpg)
![](Bokashi-06.jpg)
![](Bokashi-07.jpg)
![](Bokashi-08.jpg)
![](Bokashi-09.jpg)
