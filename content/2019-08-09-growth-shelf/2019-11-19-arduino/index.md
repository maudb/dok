---
title:  Arduino
tags: bioponic-system, electronics, programming, Fablab-Amsterdam, Fablab-Intern
---


Like I said, this project is mostly an exercise to design with electronics. Then, I chose to start with those three entities:

- a solar powering system
- a water pump
- an arduino as a timer

I keep the door open to lately add some sensors (temperature, light, moisture, ..) and some LED.

![](emma-01.jpg)
![](emma-02.jpg)

TODO: to complete

<!-- Henk gave me a solar panel with a charging board that he had in his special closet, I didn't find so much instructions about it and it made burn the board  -->
