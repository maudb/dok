---
title:  3D Filaments Holder
tags: Fablab-Amsterdam, Fablab-Intern, 3D-printing, CNC-milling
last_update: 2019-07-01
---

The idea of the project was to have a system that works both as a storage system and as a filament support just above the printers when they are printing. It was needed because Henk order the [MMU2S](https://www.prusa3d.com/original-prusa-i3-multi-material-2-0/) for the all new Prusa i3 MK3 3Dprinter that we have. This Multi Material Upgrade is an add-on which offers the possibility to print with 5 different filaments at the same time. Then we need at least 5 spots above the 3D printer.

And, because our Fablab is located in the very old and famous Waag building, the main constraint was we can not make holes in the wall. Anyway it's always better if we don't make holes at all ;)

![](Filament-holder-and-storage-system.gif)

## ALUX Frame System

We had already some [ALUX](https://www.aluxprofiel.nl) tubes here (4x 2020X800; 2x 3030x800), so we ordered the rest of the components on [Alux's website](https://www.aluxprofiel.nl/assortiment) to make a frame that will receive the filament holders and that we will be able to hang at the table where are the 3D printers.

What we ordered:

- 2x Aluminium constructieprofiel 2020 op maat - 250 mm / € 0,75
- 4x Binnenhoek - 20 profiel / € 0,79
- 47x T-slot inschuifmoer - 20 profiel - M5 / € 0,25
- 6x T-Slot indraaimoeren - 20 profiel - M5 / € 0,13
- 2x Verlengmoer - 20 profiel /€ 1,99
- 2x Verlengmoer - 30 en 40 profiel / € 2,99
- 41x Inbusbout DIN912 M5x10 / € 0,03


## 3D Printed Pieces

The Alux system doesn't have the piece we needed to create a junction in 45°. In our system, the frame is too close to the wall to have space for the filaments, and we need the filaments more above the 3D printers. This can be solved just by tilting the tube that will receive the filament holders, then I designed the pieces we needed to make this junction and printed them with the Prusa i3 Mk3 3D printer.

You can download the [.stl file](https://www.dropbox.com/s/5tbwabmd1dx7li1/3d%20ALUX%20junction%20v8.stl?dl=0) if you want to print it straight away, or the [.step file](https://www.dropbox.com/s/hac5tuwaasgyvd2/PI3MK2_3d%20ALUX%20junction%20v8.3mf?dl=0) because it's made with [Fusion360](https://www.autodesk.com/products/fusion-360/students-teachers-educators) which is a free (for students and educators) parametric software. Then, you can change and adapt the model by your own parameters.  

You need 4x M5x35mm or 4x M5x40mm to hang the 3D printed pieces to the Alux tubes. And don't forget to before put the T-solt in the tube!

![](3D-printed-piece.gif)
![](3D-printed-piece02.jpg)
![](3D-printed-piece01.png:flux)

Usually, at the Waag Fablab, we use PLA filaments (Polylactic Acid) because is a bioplastic made from renewable natural resources such as corn starch and tapioca products. In theory, PLA is easily biodegradable by composting (I never tried). But this time, we used a CPE filament (Co-Polyester) because CPE is stronger than PLA. It is a strong and versatile material with great thermal resistance and chemical resistance. It is the great material for printing mechanical parts and no chemical odors are produced during printing, like PLA.

To print those pieces with CPE with our Prusa i3 MK3 printer, we set:

- increase the printing temperature to *265°C*
- increase the bed temperature to *80°C*
- increase the retraction distance to *2.3mm*
- increase the retraction speed to *45mm/s*
- ask for *combing mode* in the travel settings
- generate a *building support*
- add a *brim* as build plate adhesion type

You can download our [preset Cura file](https://www.dropbox.com/s/hac5tuwaasgyvd2/PI3MK2_3d%20ALUX%20junction%20v8.3mf?dl=0).

## Homemade Table Clamp

We didn't find the perfect clamp table for our system, so we created one by ourself with steel angles, bolts and nuts. As simple as that. We added a wood piece to the system to block the clamp with the existing frame of the table. The other wood piece is to improve the screwing of the clamp to the table.

![](homemade-clamp-01.jpg)
![](homemade-clamp-03.jpg)


## Wood Filament Holder

We used the CNC milling machine to create our filament holder in a panel of multiplex 24mm. I designed a new model of filament holder, based on an existing version of some filament holders we had here in the Fablab. You can add the [.stl file](https://www.dropbox.com/s/926d13pdiik69e4/filament%20holder%20%28part2%29%20v2.stl?dl=0), or the [.step file](https://www.dropbox.com/s/e1i838oe9bisaex/CNC%20production_filament%20holder%20%28wood%29%20v2.step?dl=0) (for the top parts), and the [.dxf file](https://www.dropbox.com/s/h6nv8quaydck4oe/DXF_filament%20holder%20%28part1%29%20.dxf?dl=0) for the bottom parts.

![](filament-holder.gif)

Indeed, there are two parts:

  - The bottom part is just a 2D piece to cut with the CNC.

Our CNC milling machine is a [ShopBot PRSalpha](https://www.shopbottools.com/products/alpha) and we use an old version of [VCarve](https://www.vectric.com/products/vcarve-pro) to add the settings and generate the g-code.  

To do this job, we used the settings below.

Tool            | `5mm flat end 2 flutes mill`
----------------|-------------------------
Pass Depth      | `2.0 mm`
Stepover        | `2.0 mm - (40%)`
Spindle Speed   | `1800 r.p.m`
Feed Rate       | `60.0 mm/sec`
Plunge Rate     | `20,0 mm/sec`


  - The top part is a 3D piece because of the rounded edges so we used the 3D function of the CNC. Because this CNC only has 3 axes (X,Y,Z), we needed to do this job in two times: recto and verso. The two faces are the same, then we could run 2 times the same g-code. But, between the two faces, we had to flip over the multiplex panel. You have to make sure you put the panel at the exactly same place! It is recommended to put your design exactly at the very center of your panel, then use pins and marks to locate the panel on the bed of the CNC and then when you flip over your panel, the second job should be at the same place than the fisrt job.

It was our first try with the CNC and this second part was not that easy, we made mistakes so be attentive when you do this ;)

We used the same tool and same settings than the cut part for this second part.

![](filament-holders-01.jpg:flux)
![](filament-holders-02.jpg)
![](filament-holders-03.jpg)


## Prusa MMU2S

![the MMU is now in place!](prusa-MMU-01.jpg)
