---
title:  Library
tags: personal-notes, Fablab-Intern
---

### Static Website Generator

- [Jekyll](https://jekyllrb.com/)  
- [Hugo](https://gohugo.io/getting-started/quick-start/)
- [MkDocs](https://www.mkdocs.org/)


### FabLab Ressources

- [fablabs.io](https://www.fablabs.io/)
- [FabAcademy](https://fabacademy.org/)


### Wiki & DIY kit

- [KOBAKANT](https://www.kobakant.at/DIY/) DIY Wearable Technology Documentation
- [wikifactory](https://wikifactory.com/discover)
- [learn.sparkfun](https://learn.sparkfun.com/)
- [Bare Conductive](https://www.bareconductive.com/) Printed Electronics Technology
- [Sensorica](https://www.sensorica.co/home)
- [Soft Robotic Tool Kit](https://softroboticstoolkit.com/)
- [Textile Academy - soft robotic class](https://class.textile-academy.org/classes/week11/)
- [Bodymimicry - Soft Robotic project](https://class.textile-academy.org/2019/Montserrat/project.html)
- [Fritzing](https://fritzing.org/projects/by-tag/pump/)
- [The Machines that make](http://mtm.cba.mit.edu/)
- [Instructables](https://www.instructables.com/workshop/)


### Online Tools

- [MakerCase](https://www.makercase.com/#/)
- [boxes.py](https://www.festi.info/boxes.py/)
- [gcode Viewer](http://gcode.ws/#)
- [CNC panel joinery](https://makezine.com/2012/04/13/cnc-panel-joinery-notebook/)


### 3D bank & support

- [thingiverse](https://www.thingiverse.com/)
- [Prusa](https://help.prusa3d.com/en/)
- [simplify3d (print-quality-troubleshooting)](https://www.simplify3d.com/support/print-quality-troubleshooting/)


### Tutorials

- [Soldering is easy](http://mightyohm.com/files/soldercomic/FullSolderComic_EN.pdf)
