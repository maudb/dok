---
title:  Make a Laptop Stand
tags: Fablab-Amsterdam, Fablab-Intern, lasercutting
---

The goal of this exercise was to do something quite simple to have a first contact with machines like :

- laser cutter
- 3D printer
- vinyl cutter

and useful like a laptop stand.

☞ Here is a reference : [kolibri by cremacaffé](https://cremacaffe.shop/collections/frontpage/products/kolibri)

☟ Here is an existing project in the fablab of a standing desk

![](standingdesk001.jpg)
![](standingdesk002.jpg)


I designed the model on FreeCAD (an open-source parametric 3D modeler). It was my very first step with this software. I had good help with [those tutorials from CAD Printer](https://www.youtube.com/channel/UChwUxlPx6EP4hKFQyA4rCuQ/playlists) (in French) to understand well the software.

You can find my files [here](https://www.dropbox.com/sh/9cn27noji942t5i/AABqfmRexKdRdkbg_us8B4L_a?dl=0).

- Version 1 : the two freeCAD files (one for each part) *NB: you can adjust measurements for custom because it's a parametric design !!* and the .svg export
- Version 2 : the .ai and .svg exports for the adjustments I did hereafter (see below).

I cut my first try with the laser cutter (a CO2 BRM Lasers) in a 4mm light triplex with those settings:

*Kerf* : `0.15 mm`

*Speed*     |  *Power*    | *CornerPower*
------------|-------------| -------------
`100`       | `100`       | `20`

And here is the result:

![and the final result](laptopstand001.jpg)
![you can see my cut tests on the top of the wood sheet to find the right setting (because it depends of the material used)and find the kerf to adjust the measurements of the cuts for the interlocking of the two parts](laptopstandGIF001.jpg)

I then had an amelioration on the first version to have only four points of contact with the table. Because with lines, when the table is not perfectly flat, there are some movements.

To save materials, I recut my two parts with the lasercutter instead of cutting new parts. It was not that easy to be precise because this lasercutter doesn't have origin setting, so it's quite impossible to continue a job that you made before. You can only see by eyes if you point the head of the laser at the good place.

To keep the possibility to modify your pieces, a good way is to design a frame around your model. Then, each time you want to add new cutting or engraving, you can place it inside the frame and the origin of the laser will be the same. But it only work when you do all that steps straight away without moving the head of the laser.

![](laptopstand002.jpg)

For the next step, I want to upgrade this laptop stand to make it a bit bigger and remove material below to save space and put some thing under the laptop.
