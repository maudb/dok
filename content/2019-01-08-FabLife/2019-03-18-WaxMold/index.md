---
title:  How to Reuse Machinable Wax
tags: Fablab-Amsterdam, Fablab-Intern, machinable-wax, lasercutting, mold-and-casting
---

Here at Waag Fablab they use [bricks of Machinable Wax](https://www.machinablewax.com/index.php) to make mold with the CNC milling machine. They always bought new ones but is technically possible to remake bricks with the leftovers wax dust and the old used bricks by melting them.

See below how we did it.

## Create a reusable casting box

![We found this found this system very simple and adjustable](wax-mold-example01.jpg)
![So, we made our own version, quite the same](wax-mold-acrylic05.jpg)
![](wax-mold-acrylic01.jpg)

You just need:

- a sheet of acrylic (of a sheet of wood if you want something cheaper, but it must be varnished or something),
- 4 folders clamps
- and a tube of acrylic glue (or wood glue for the wood)

You can download our [.svg file](https://www.dropbox.com/s/c27p8tpopxkzmda/mold%20box.svg?dl=0) if you can have access to a lasercuter to cut the different pieces.
The red lines supposed to be cut, the black lines supposed to be engraved, and the triangle pieces on the bottom of the file are there to strengthen the corners. The pieces are connected to each other by interlocking joints, but it's better to glue everything for better handling.

We used a kerf compensation of 0,5mm in our file.
![We found the kerf compensation after doing those tests](wax-mold-kerftest01.jpg)

Here in the fablab, with our Lasercuter (a CO2 BRMLasers), we use those settings with a sheet of 3mm of acrylic :

mode | Speed | Power |
-----|------|-------|
Cut (red) | `25` | `80` |
Engrave (black) | `500` | `25` |

![](wax-mold-acrylic03.jpg)
![](wax-mold-acrylic04.jpg)
![](wax-mold-acrylic05.jpg)
![PS: You can use clamps for the bottom aswell](wax-mold-acrylic06.jpg)

## Melt the wax

Put the wax in a pan, toss and wait. (for a long time, be patient!) The wax is supposed to melt at 70°C. We didn't have thermostat or thermometer for our cooking plate, so we turn on the button to the middle and it worked (after few mistakes, see below for some advices ;)

<video><source src="wax-mold-video-01.mp4"></video>

### CAUTION

- don't increase the temperature too much! Otherwise the wax boils and burns
- don't decrease the temperature too quickly! Otherwise you lost the heat and the wax freeze  
- it's easier and faster if you cut the wax into little pieces, or if you can grind it
- don't stop tossing! Otherwise the top layer freeze  
- just be patient, it's coming
- the bain-marie technique was a failure for us
- the microwave technique is maybe to dangerous (see pictures below)

![With this yoghurt jar of glass, the wax melted after ~20min at 600W in the microwave. BE CAREFUL, THE GLAS IS EXTREMELY HOT](wax-mold-microwave01.jpg)
![We used another glass jar to alternate and let the previous cool, but this one explode in the microwave!](wax-mold-microwave02.jpg)


## Mold the new bricks of wax

Put the hot wax into the casting box and wait until the outside of the wax are strong enough. Remove the folder clamps and wait until the new brick of wax be totally cold (~12 hours)

<video><source src="wax-mold-video-02.mp4"></video>
<video><source src="wax-mold-video-03.mp4"></video>

![NB: Sticky paper was used to secure the bottom to the support to ensure the mold remains in place and to prevent leakage](wax-mold-bricks00.jpg)

And let's reuse! And recycle again after reuse!

![](wax-mold-bricks01.jpg)

And store the system when you're done.

![](wax-mold-acrylic07.jpg)
