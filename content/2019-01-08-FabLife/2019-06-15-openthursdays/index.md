---
title: Open Thursdays
tags: Fablab-Amsterdam, Fablab-Intern, open-day, lasercutting, 3D-printing, 3D-scanning, CNC-milling, vinyl-cutting
featured: True
featured_image: openday-01.jpg
excerpt: introducing visitors and interested critical makers about the fablab philosophy and the ongoing projects of the Waag
---

Each Thursday from 12:00 to 17:00, the Waag Fablab is open to the public. During the open Thursdays, we do a tour of the space and the three different labs (fablab, wetlab, textilab). We explain how the machines work, what you can make with them and what is the philosophy behind the maker movement and digital fabrication. We also speak about the Waag, its philosophy and its ongoing projects, and we show some hidden treasure in the building.

Critical makers with interesting projects are always welcome aswell as just curious visitors. We are happy to answer any questions or helping find your way in the Amsterdam maker scene, as there is a growing number of so-called 'Makerplace' in the city (like at the public libraries OBA). Some projects could also be developed and produced during the open Thursday.


![](openday-10.jpg:flux)

[Debby](https://www.dmarchena.eu/) who is a partially sighted woman, wanted to make her own perfectly fit night mask because she is very sensitive to day light when the sun rise up. We 3D scanned her face with the 3D sense scanner we have here, and we milled her face in foam with the CNC to give her a model to make prototypes.

![](openday-09.jpg)
![](openday-11.jpg)


[Dan](http://buzzo.com/) who is an artist, designer and researcher, wanted to mill the [Open Source Beehives project](http://greenfablab.org/opensourcebeehives/) from the Green Fablab of Barcelona to use it in his land in the forest of Belgium and collect the data of the activity of the bees via the sensors.

![](openday-14.JPG)
![](openday-13.jpg)
![](openday-15.jpg)

We also hosted some teenagers for one day internship in company. With them, we played with lasercutting, 3D scanning, 3D printing, vinylecutting and soldering.

<video><source src="openday-00.mp4"></video>
![](openday-01.jpg)
![](openday-04.jpg)
![](openday-05.jpg)
![](openday-07.jpg)
