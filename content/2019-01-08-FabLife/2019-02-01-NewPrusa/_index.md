---
title:  New 3D Printer
tags: Fablab-Amsterdam, Fablab-Intern, 3D-printing
---

Waag Fablab has few 3D printers, but none works properly.

- *3Dprinter BCN3D* : only one of the two extruders works but the print quality is average.
- *3Dprinter PrinterBot* : the nozzle is clogged and it's the second time in a short period. Anyway, it's also an old boy.
- *3Dprinter Tevo Tarentula* : This is a donation. It's an open-source printer and it needs some upgraded parts to keep it stable because it's impossible to have a good print for now.

Conor already passed 2 weeks on each printer to try to fix them before I came but it takes time. Then Henk decided to buy a new 3D printer because the lab needs a stable one and the FabAcademy (a six months program to learn everything about and around digital fabrication) will begin soon.

He choose the [*Prusa i3 MK3*](https://www.prusa3d.com/original-prusa-i3-mk3/) and order it in a kit to assembly it here by Conor and I in order to save money and to learn better how to work the printer.

## Building

The assembly part was long but easy thanks to the [instructions](https://manual.prusa3d.com/c/Original_Prusa_i3_MK3_kit_assembly).

![](Prusa-01.jpg)
![](Prusa-02.jpg)
![](Prusa-03.jpg)
![](Prusa-04.jpg)
![](Prusa-05.jpg)
![](Prusa-06.jpg)

## First tests

After all the assembling and the preflight check, we were ready to run some first tests.
We did first a tiny model downloaded on [Thingiverse](https://www.thingiverse.com/) which was offer good result, so we were ready for something bigger.

### My Goldy face

I choose to print a scan of my bust that I did before with the [Sense 3D scanner](https://www.3dsystems.com/shop/sense) we have here in the Fablab. I choose this [Filamentum PLA Extrafill "Gold Happens"](https://fillamentum.com/collections/pla-premium-filament/products/fillamentum-pla-extrafill-gold-happens). We mostly buy PLA filament here because it's made of natural ingredients.

To generate the g-code (which is the coding files that the 3Dprinter can read), you need to load your 3D file with the .stl extension in a software which translates the 3D files in X,Y,Z coordonates. Then the 3D printer knows what to do.

The Prusa 3D printers have their own software to generate the g-code which is [PrusaSlicer](https://www.prusa3d.com/prusaslicer/) (formerly known as Slic3r Prusa Edition or Slic3r PE).

For this test we run the 3D print of my bust with the draft quality (height of layer : 0.2mm), it took about 1 hour of printing for a model's height of 45mm (10% of my real size) with a 20% infill density. And the result was really good even with the draft quality, we can see all the details of my hairs and my turtleneck! The quality of the filament and the quality of the printer makes together really good print.

![](Prusa-07.jpg)
![](Prusa-09.jpg:flux)

### Flexible Mini Mouse

For the second test, we wanted to use the [Filamentum Flexfill 98A "Sky Blue"](https://fillamentum.com/collections/flexfill/products/flexfill-98a-sky-blue?variant=1260388659) wich made with Flexible TPU. We run first a tiny [mouse from Thingiverse](https://www.thingiverse.com/thing:61909) (scaled in 50% to save some time) with the draft quality (height of layer : 0.2mm).

It's a good print but not very flexible, probably because of the infill density at 20% and because of the grade ShoreA 98 wich is quite strong for flexible stuff. Next time, we will reduce the density of the infill to improve the flexibility.

![](minimouseflex-test001.jpg)

### Flexible Conor bust

The idea was to print Conor bust in flexible then we can crush it. But after few layers of print, the filament got stuck in the pulley of the extruder. So nothing was coming out the nozzle properly from this point.  

![](connorfaceflex-test001.jpg)

### Flexible Mouse test 1

We tried to print a new test, to find where was the problem with the print of Conor bust and take the opportunity to reduce the infill to have more flexibility.

We did a mistake last time with the Conor bust, we print with the PLA's filament preheat setting of the printer, so the heat of the nozzle was only at 215° (and the heat of the bed at 60°, the model was very difficult to remove). There is a Flex's filament preheat setting in the printer with a nozzle's heat at 240° and a bed's heat at 50°.

It seems that it is better to print flex fil with a higher temperature but again, the filament got stuck in the pulley of the extruder. We saw that the problem came at the same time, when the printer begin the infill, after the firsts layers.

![](mouseflex-test001.jpg)

### Flexible Mouse test 2

We did a second test by reducing the print speed. Unfortunately, it was not better, still the same problem than the last one.

![](mouseflex-test002.jpg)

But there is an issue: when we changed the speed setting in slic3r, the duration of the print didn't change so much. Which makes no sense.

### Flexible Mouse test 3

We run again the last g-code and we tried to print the mouse by changing the speed directly on the machine with the "Tune" option on the menu of the 3Dprinter.

Good news, it worked ! The problem was not anymore with the filament. By reducing the speed in half, the filament didn't get stuck anymore which makes sense because the flexible filament is like a cooked spaghetti. So, when you try to put a cooked spaghetti in a hole, you need to be slower than with a dried spaghetti. CQFD

But now the problem is the extrusion quality:

![](mouseflex-test003.jpg)

When we have a look on [Simplify3d](https://www.simplify3d.com/support/print-quality-troubleshooting/) which has a nice support section, it seems that we have 2 problems at the same time:

- "Stops Extruding Mid Print"
- "Weak Infill"

After a quick check on the nozzle, the problem is not the extruder and we already try different speed setting.

Pim (who is another intern at Waag) said that a friend of him has the same 3D printer and that he tried to print things with flexible filament around 50 times and it only worked 1 time even after by using the right setting he used before. So his best advise is to give it up ....

## Advices to 3D print with Flexible filament

But according to [this video of Maker's Muse](https://www.youtube.com/watch?v=fTJz6vMvtJ8), to [this article from Prusa](https://www.prusaprinters.org/how-to-print-with-flexible-filament/), to [this article from simplify3D](https://www.simplify3d.com/support/materials-guide/flexible/) and to Pim's friend comments, print something with flexible filament should be work by :

- change the initial layer heigh to a good save distance away from the bed (not as close as PLA)
- increase the print temperature (=raise the recommended hotend temperature to 5°C higher)
- disable retraction of the nozzle
- increase extrusion multiplier (=flow)
- decrease the print speed (=typical safe speed is 20mm/s)
- avoid crossing outlines (=combing mode)
- disable support & raft or set the gap between the layers in Z-axis to at least 0,3mm


## Advices to 3D print with CPE filament

If you have trouble to print with CPE filament, I suggest you to try some of those advises below from Flexible filament. It worked better for me.


## Advices to 3D print with support (to make the support easier to remove afterward)

- overhang threshold : 75°
- Contact Z distance : 0.2 (detachable)
- Pattern : Rectilinear grid
- Pattern spacing : 4mm
- Interface pattern spacing : 0.3mm
- Don't support the bridges : uncheck

![](large:slic3r-support.jpg)
