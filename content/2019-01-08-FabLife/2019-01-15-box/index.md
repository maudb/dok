---
title:  Make a Box
tags: Fablab-Amsterdam, Fablab-Intern, CNC-milling
---

![The CNC milling machine at the Waag Fabla](CNC.jpg)

Today, Henk (the manager of the lab) introduce us to this big CNC milling machine.
So now, we need a project to use this one.. Conor (another intern at Waag Fablab) and I really need something to storage our stuff here. So we think about making a box.

A previous intern made this one ☟ and it is very nice, with a "clips" system.

![](internbox.jpg)
![](internboxzoom.jpg)


I also like this website ☞ [opendesk.cc](https://www.opendesk.cc/zero/pedestal#get-it-made) where you can download open plan designed by contributors to make furniture with a CNC milling machine.
For example, here is a [pedestal](https://www.opendesk.cc/zero/pedestal#get-it-made) which can also be a good start to design our own box.

In the same idea, [Libreobjet.org](http://libreobjet.org/) is a group of designers and hackers. They all share a common question about open source industrial design, processes and products resulting from them work, with the aim of providing tools for accessing a free philosophy applied to the manufacture of objects. So there, you can  also download open plans and production guides. And even propose an updated version or a new project.

here is a good read to learn a bit more about open-design and open-licenses : [Diverted Derived Design](https://books.libreobjet.org/).
