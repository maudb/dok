---
title: GitLab Page
tags: gitlab, personal-note
---

# GitLab

## GitLab Page
- https://about.gitlab.com/stages-devops-lifecycle/pages/
- https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/
- https://docs.gitlab.com/ee/user/project/pages/index.html#getting-started-with-gitlab-pages
