---
title: About this website
tags: python, static-site-generator
---

This website is a custom static site generator written in Python by [Antoine J.](https://antoine.studio) All sources are available on [his GitLab](https://gitlab.com/antoinestudio/dok), the project is called DOK.

We can say that this website is low-tech and sustainable because :

- it is a series of html pages without any call to a database (= static site)
- there is no javascript
- the CSS is written to be lightweight
- the images are compressed and the videos are self-hosted

+ there is a RSS feed
