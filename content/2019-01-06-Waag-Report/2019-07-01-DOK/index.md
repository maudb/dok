---
title:  Documentation Tool
tags: Fablab-Amsterdam, Fablab-Intern, opendesign, co-creation, static website
last_update: 2020-09-17
---

## About Open Design and Co-creation

The Fab Lab movement is closely aligned with the DIY movement, open-source hardware, maker culture, and the free and open-source movement, and shares philosophy as well as technology with them.

While fablabs have yet to compete with mass production and its associated economies of scale in fabricating widely distributed products, they have already shown the potential to empower individuals to create smart devices for themselves. These devices can be tailored to local or personal needs in ways that are not practical or economical using mass production.

The FabLab Network is an open, creative community of fabricators, artists, scientists, engineers, educators, students, amateurs, professionals, ages 5 to 75+, located in more than 40 countries in approximately 1000 Fab Labs. (according to fablabs.io)

To contribute to this movement, it makes sense to create a website as a tool of communication.

## About Static Website

Static sites are the most basic type of website and are the easiest to create. Unlike dynamic websites, they do not require any web programming or database design. A site can be built by simply creating a few HTML pages and publishing them to a Web server.

To avoid coding everything from scratch, it exists a lot of open source static site generators that you can install on your computer and generate your own static site. You can also choose and install themes created by others to quickly have something that suits you and looks good.

After a quick start with [Jekyll](https://jekyllrb.com/), I finally choose [Hugo](https://gohugo.io/) to build my static website for the following reasons:

- My boyfriend Antoine is web designer, and after few chats, we wanted to develop our own documentation tool. We thought about a system together and Antoine then developed it. (It's the one you see right now by the way). But it was more difficult than expected with Jekyll.
- to make a short story of it, Jekyll is a one of the first open-source static website generators and it's written by Tom Preston-Werner who is one of the GitHub's co-founder. Because GitHub has just been acquired by Microsoft, it makes it less attractive. Anyway, it was a brain teaser for Antoine to work with Jekyll and since jekyll was created, a lot of following open-source static website generators were born with interesting solutions.
- For example, Hugo was easier to install.
- The taxonomy system is more flexible which makes the code easier to adapt and modify.
- And the compilation speed is higher which makes the generate of the site faster.

To install Hugo on my computer, I just followed the [instructions](https://gohugo.io/getting-started/quick-start/) on the website. <br>
PS : It was my very first use of the terminal of my computer! ;)

Now, each time I want to run my website on my computer, I write this on the terminal :<br>
`cd` followed by the name of my folder <br>
`Hugo serve` or `Hugo serve -D` to show the drafts<br>
and then navigate to my site at http://localhost:1313/

I write the content of this website in [markdown syntax](https://www.markdownguide.org/basic-syntax) with [atom](https://atom.io/) which is a code editor.

TODO: update > This website is now built with a custom static site generator that Antoine wrote in Python. Sources are available on [Gitlab](https://gitlab.com/antoinestudio/dok-antoine-studio).

## About GitLab

GitLab is an open-source web-based Git-repository manager with wiki and issue-tracking features. It means that it offer you:

- hosting for all the code and the content of your site.
- versioning. Each time you load new version or new content, it keeps the old versions then you can have a step back if it's necessary.
- And it's open source then you can see everyone project and use part of it or all of it inside your own project.

Basically, in my case, I downloaded the framework of this site on [Antoine's GitLab page](https://gitlab.com/antoine.j/dok). I opened the folder on atom and I wrote my content. Then I uploaded it on the [Waag Intern Gitlab page](https://gitlab.waag.org/).

Each time Antoine does modifications of the code, I can download the new version and update my site. And each time I had new content, I upload the new version on Waag Intern Gitlab.


### How to push new content on Gitlab

You first have to create a branch between your Git Repository (the folder on your computer which contains all your website) and the gitlab server. For that, you need to create and add your SSH public key. All the doc can be found on [GitLab Docs - basics](https://docs.gitlab.com/ee/gitlab-basics/README.html)

Then, I can push my content by the terminal with those commands:

`git status`

`git add .`

`git commit -m "message"`

`git push`

or all at the same time: `git add . && git commit -m "message" && git push`

I can also do it directly on Atom, with the Git Function by following the instructions:

1. Stage all
2. Give a commit message
3. Commit to master
4. Push

![](DOK-system-GitLab05.png)


### How to pull updates by the author from Gitlab

TODO: how to pull updates from gitlab


## About the content of this website

Here, I will explain all my personal projects and all that I did during my stay at Waag FabLab Amsterdam so that you can understand each step of the process, the difficulties encountered and the solutions found. My documentation is meant to be read and to give everyone the opportunity to learn, to be inspired and perhaps to participate.
