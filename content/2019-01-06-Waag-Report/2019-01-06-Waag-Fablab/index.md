---
title:  Waag Fablab Amsterdam
tags: Fablab-Amsterdam, Fablab-Intern, Waag, Report
---

## About Waag

[Waag](http://waag.org/) operates at the intersection of science, technology and the arts. Their work focuses on emergent technologies as instruments of social change, and is guided by the values of fairness, openness and inclusivity. Waag’s dedicated team of sixty thinkers and makers empowers people to become active citizens through technology.

Waag is a middle-ground organisation composed of research groups that work with both grassroots initiatives and institutional partners across Europe. The collective has a shared attitude of public concern and civic activism, which is manifested in their public research agenda. Working with emergent technologies, Waag conducts research in both imaginative and practical terms, addressing its fellow citizens from a position of equality and collaboration.

They do Public Research which consists of the following groups:

- *Make* has a DIY attitude, researching societal and ecological questions through hardware, production processes and materials.
- *Code* works to raise awareness of the consequences of new technology and develops concrete alternatives to make citizens more resilient and agile.
- *Learn* focuses on contemporary education and heritage and explores experiential disciplines to help people meaningfully participate in society.
- *Care* uses co-creation to work with users, designers, artists and developers to research and develop innovative concepts for the healthcare sector.


## About FabLabs

Inside the Waag, there is a Fablab. A Fablab is a place for learning and innovation: a place to play, to create, to learn, to mentor, to invent. The concept of Fablab was developed by Neil Gershenfeld from MIT during a class 'How to make almost anything'. It's an abbreviation of Fabrication Laboratory.

Fablabs are a global network of local labs, enabling invention by providing access to tools for digital fabrication. Over ten years ago, Waag opened the doors of Fablab Amsterdam which is situated on the first floor of the Waag monument in Amsterdam. It was the first Fablab in Europe. Waag's Fablab is a member of a global community of learners, educators, technologists, researchers, makers and innovators.

More info on [fablabs.io](http://fablabs.io/)

At Fablab Amsterdam, Waag offers workshop formats for education and the Academy programs in the field of digital fabrication, biotechnology and textiles/new materials. They also use the Fab Lab as a rapid prototyping facility for their own projects.

Fablab Amsterdam also participates in the international Fab Academy, a distributed course in digital fabrication from MIT.

Waag regularly schedules events that use the facilities of the Fablab.

On Open Thursdays they explain how the machines work, show what visitors can make in a Fablab, and share the philosophy behind the maker movement and digital fabrication.

More info on [fablab.waah.org](http://fablab.waag.org)

![](Report-DeWaag.jpg)

## About my goals

As a trainee here, I will develop my interest in open design, digital fabrication and open hardware and learn how to work in a Fablab and use its machines.
In Waag’s Fablab Amsterdam, they need an hands-on intern to support ongoing projects and the Open Thursday program. Moreover, they provide to me with tools, machines and support to develop my own creative project and produce prototypes. Additionally they will introduce me to the international Fablab network and Amsterdam maker scene.

Fablab Amsterdam is a place for creativity, craftsmanship and digital experiments, where you can make pretty much anything. It provides tools and machines to build sensors, electronic circuits or 3D print an array of beautiful products.

They are also interested in new forms of recycling and sustainable use of materials. I want to guide my projects on that direction.

On Open Thursdays I will explain how the machines work, show what visitors can make in a Fablab, and share the philosophy behind the maker movement and digital fabrication.

I will learn how to use the main machines: laser cutter, CNC milling machine, 3D printers, vinyl cutter and others. I will have access to the Fablab during working hours and a mentor throughout my internship.

I have the feeling that, here, it is a place where I can learn a lot in a short time and where I can work in a young, dynamic and flexible work environment. I will have a lot of responsibility and there is plenty of room for my own initiative and creativity. It is expected I will fully support the team during my internship at Waag

For my own personal project here, I have the possibility to :   

- Learn project development and management
- learn modeling with 2D and 3D software
- learn 3D printing and scanning
- learn CNC machining in small and large scale
- learn laser cutting
- learn mould design, construction and casting
- learn website development tools
- learn version control protocols
- interpret and implement networking protocols
- learn design and programming circuit boards
- learn the use of sensors and output devices
- learn interpret and implement programming protocols
- learn mechanical and machine design
- learn integration of techniques into a final projects

At Waag Fablab, they ask me in exchange to :

- Managing all machines and softwares at the fablab
- Assist visitors or Waag employees with software and machining
- Contribute to a safe and pleasant work environment
- Assist with cleaning, maintaining and improving the lab
- Contribute to open source documentation by documenting and sharing the development of my own project
- Doing tour during the Open Thursdays

![](Report-FabLab.jpg)
