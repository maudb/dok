---
title:  Conclusion
tags: Fablab-Amsterdam, Fablab-Intern, Report
---

I loved my internship at the Waag FabLab in Amsterdam!

Yet I was completely out of my comfort zone.I came into a much more technology-oriented environment than I used to sit and tested my level of English. I started almost from scratch after working 4 years in an architecture office but that's exactly what I was looking for to make me evolve professionally and personally.

Despite a few weeks full of informations, I quickly adapted to the work rate of the fablab.

I was very well received and supported by Henk, the Waag FabLab manager, who directly gave me a tour of the FabLab and its adjacent labs (WetLab and TextileLab), as well as all the other historical surprises offered by the Waag building. During this tour, I also met the entire Make section team.

I also met other trainees from all around the world with whom I was able to quickly establish links. There is a lot of going and coming to Waag because there is also a course program that welcomes about ten students for 5 months. I was surrounded by other people who, like me, had just arrived in Amsterdam (or at least in the Waag).

I spent the first months of my internship learning how to use the machines and assist other people (employees or students) in their projects. Once the course programs were completed, I then had more time and skills to develop my personal projects. From then on, Henk let me fly on my own but remained available.

I interacted with other people who come and go at the FabLab, I learned by doing, I failed and I started again.

I feel like I have learned a lot in a very short time, I feel comfortable now in a FabLab and my level of English has evolved. I would like now to have the opportunity to continue to develop my personal projects and to explore the electronic part that can be done at the FabLab.

That's why I extended my internship at the Waag until November, this time accompanied by European Solidarity Corps. And the stars line up because friends of friends are lending us their apartment located on Amsterdam's Java Eiland for the next four months.
