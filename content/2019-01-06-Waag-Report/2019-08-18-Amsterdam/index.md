---
title:  Amsterdam
tags: Amsterdam, Waag, Report
---

## The city of Amsterdam

<iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=4.842395782470704%2C52.33376566719132%2C4.959125518798829%2C52.399381523533336&amp;layer=hot" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=13/52.3666/4.9008&amp;layers=H">OpenStreetMap</a></small>

What I really like here is that Amsterdam is a city on a human scale where it is easy to get around everywhere (thanks to the flatness of the Netherlands). It's like being in Brussels for that (except the flatness) but Amsterdam is really much better organized! For bike paths, for parks and all other public spaces. There are fewer cars and a city with water and tall trees makes me feel good.

Amsterdam also has the energy of a capital and there is so much to do and see. Many new approaches and alternatives, I have the feeling that things are moving forward here while keeping the solid foundation. Here I have developed my interest in new (or old) ways of designing and for sustainable alternatives. I also paid attention to architectural, graphic or musical audacity.

I also choose this destination because it is a Dutch-speaking city. I knew that my internship would be in English but I also wanted to improve my rusty Dutch outside the internship. What was not easy! The Dutch are all bilingual and love to speak English, even in everyday life. There are also a lot of English speaker expatriates. The Dutch imersion was therefore more difficult than expected.

## The housing in Amsterdam

It was very complicated to find accommodation in Amsterdam. Indeed, it is a nice little town where the Dutch like to live, where tourists love to spend their time and where there are many expatriate workers, because Amsterdam is one of the main financial centers in Europe.

My boyfriend and I wanted to find a one bedroom apartment or a room in a sharing apartment inside the ring of Amsterdam (to enjoy the city with our bikes) with a max budget of 1.000€ incl. /for two.

Shared groups on Facebook were our best way of research. We started the search since Brussels in December and after **a lot** of negative responses, we finally found a one bedroom apartment located in the Hoofddorppleinbuurt (Zuid) but only for the month of January (first month of my internship). It was the apartment of a German-Belgian couple who goes on Hollidays during this period.

It punctuated the rest of our stay in Amsterdam and we had to put 250€ more on the housing budget. We spent the next month in a two bedrooms shared apartment located in the Oostenburg (Oost) with an English roomate. The French girl whose we took the room was traveling during this period. Then we moved for two months in a one bedroom apartment located in the Plantage (Oost) from a Dutch girl with broke up problem and finally go for the last two months in another one bedroom apartment located in the Frederik Hendrikbuurt (West) also from a Dutch woman who was in India for yoga retreat.

It was a pain in the ass to look for accommodation every month or every two months, but finally we were happy with this situation because it gave us the opportunity to discover several corners of the city and carry with us, in our suitcases, that what we really needed.


## De Waag

A bit of history, the Waag is the oldest non-religious building in Amsterdam dating back to the 15th century. And before hosting today an organization making technology and society more open, fair and inclusive (where I did my internship), the Waag was first a gate of the city and part of the Amsterdam wall.

![](Report-Amsterdam01.jpg)

Then, in the 17th century, the old city gate was transformed into a weigh house, a public building in which different goods were weighed (the name "waag" comes from there). A number of guilds were located on the upper floors of the building: the Blacksmiths' Guild, the Painters' Guild, the Masons' Guild, and the Guild of Surgeons. Each guild had its own gateway. Guild emblems are always visible on these entries. A very famous painting by Rembrandt, "The Anatomy Lesson of Dr. Nicolaes Tulp" shows this time.

After falling into disuse as a weighing house, the Waag performs various functions. In the 19th century, it was used successively as a closing room, furniture workshop, oil lamps workshop for public lighting, fire station and archives of the city. In the first half of the 19th century, punishments were made in front of the building. There was even a guillotine.

In the 20th century, the building was mainly used as a museum.

Finally, in the 21st century, after some serious renovations commissioned by the municipality, a call was made to occupy the building. Waag Technology & Society is located on the upper floors. The ground floor is a café and a restaurant.


## Facts and tips

### See below 3 facts I noticed during my stay in Amsterdam (from January to July)

1. You certainly see in the photos of Amsterdam traditional houses with large windows, it's beautiful and it's really nice when you're inside. But the most surprising is that when night falls, no one closes his curtains as they do in Belgium (They even don't have curtains). You can see people at home through the windows. It's really confusing because you want to look inside but you do not want to be rude. We finally learned that it comes from the Orthodox heritage. "A good citizen has nothing to hide" so at the time, if your curtains were closed it seemed suspicious. The Dutch have kept this habit. I would say that, in general, the Dutch live in harmony with the public space. It is common to see people sitting in front of their doorstep to catch a ray of sunshine. doorstep that is usually landscaped with plants and benches, it really makes the city nicer.

![](Report-Amsterdam02.jpg:flux)
![the exception that confirms the rule: sometimes some people have curtains ;)](Report-Amsterdam03.jpg:flux)


2. In Amsterdam, there is no electric shared scooter and no electric shared bikes. I think this is better because I have the feeling it is a real mess in Brussels. But here, there is a lot of bikes with a blue front wheel. It quickly puzzled me and I finally solved the riddle: this is a brand of swap bikes (swapfiets.nl). You get your own bike for a fixed monthly fee and you can exchange it for another one as soon as you have a repair to do. It's a good deal for expatriates and many young Dutch people also have theirs, but I think is not a sustainable idea because you lose the sense of responsibility, you take less care of your bike and your not learning basic knowledge like how to repair a flat tyre or or how to repair a chain derailed. Unfortunately, this kind of problem is appearing in more and more domain and we have to remain critical in relation to the new digital facilities.


3. Amsterdam today has many former squats that play an important role in the cultural and alternative offer of the city. Antoine and I used to visit many of their events while our stay in Amsterdam (concerts, exhibition, vegan dinner, karaoke, talk, festivals, yoga class, film show, etc). We found many of them on [radar.squat website](https://radar.squat.net/en) or directly on the online agenda of each former squat. they are all listed on [culturelestelling.amsterdam](https://culturelestelling.amsterdam/). And most of the fees are on donations!  

*Learn a bit more about krakers en anti-krak (squats et anti-squat)*

*In the 1960s, urban planning projects emerged and renovation of the old city included demolitions and reconstructions related to the aftermath of the war and the recent construction of the metro. It also includes in a second time the passage of a highway. People are mobilizing against these modernist projects, against the disappearance of heritage and against the reduction of the number of housing in a country where the lack of living space is a recurring problem. Taking advantage of empty spaces thanks to the urban plan, the squatters commit in the threatened future of their neighborhood. The struggle becomes common and after claims and dialogues, new urbanistic principles emerge in the late 1970s. The highway project is canceled and social housing are built with a participatory process.*

*Between 1980 and 1985, there are about 20,000 squatters!*

*Today, many squats are legalized, bought by the municipality and rented for modest rents. Activism is less fervent but the energy is put in the development of the cultural offer which is mentioned in program of the city of Amsterdam.*

*This could be seen as an institutionalization of culture for economic and real estate purposes. But these lab spaces work spaces and living spaces are growing. And the little interference of the municipality ensures freedom and a mix of cultures inside the city. The same principle is also implement for the vast land used as nurseries.*

*This phenomenon has resulted in the anti-krak (anti-squat) because since 1990, the squat movement is supported by a flexible law: In the Netherlands a building vacant for a year can be legally squatted if the owner can not demonstrate a project of use in the following months. So, to avoid the occupation of their property, some owners offer a small rent in exchange for protection against the squat. Intermediary associations manage the tenant-owner relationship. Some families or students have made their housing strategy. For example, Conor, who is another intern at Waag live in an anti-squat for a rent of 300€ incl. This is a former hospital that is waiting for a reassignment project.*


### See below 11 recommendations of things to do in Amsterdam

- Come at Waag (Nieuwmarkt 4) on Thursdays from 12:00 to 17:00 for a Fablab tour, then get a drink at the Fonteyn just on the other side of the street, later, catch a Suriname snack at Tokoman on the other side of the Waag and come back at 20:00 to follow one of the [varied events](https://waag.org/en/events) programmed by the Waag.

- Go to play ping-pong on Tuesday from 21:00 to 01:00 at the former squat [OT301](http://www.ot301.nl/) (Overtoom 301), and if you like, you can first have a 3 services Vegan Voku Dinner at the OT301 cafe. Only on RSV

- Join one of the [mediamatic](https://www.mediamatic.net/) workshops (Dijksgracht 6) dedicated to new developments in the arts, focusing on nature, biotechnology and art+science. Or just have a seat with a nice view and choose one of their beautiful and delicious sparkling lemonades from the restaurant.

- Eat unlimited veggie or vegan pizza and pay what you like on Fridays at [de KasKantine](http://www.kaskantine.nl/) (Vlaardingenlaan 100), a urban farm café.

- Find a Dutch friend with a boat and do a tour on Amsterdam canals, see the city from a new viewpoint, and finally jump in the Amstel River.

- Spend a club night in the basement of [De School](https://www.deschoolamsterdam.nl/en/) (Dr. Jan van Breemenstraat 1 ). De school is a former school converted in an "Art-Cafe-Club-Concert-Gym-Restaurant".

- Visit [De Ceuvel](http://deceuvel.nl/en/) and discover how they turn a former polluted shipyard site into a regenerative urban oasis. It's one of the most unique urban experiments in Europe. Old houseboats have been placed on heavily polluted soil, the workspaces have been fitted with clean technologies and it has all been connected by a winding jetty. Around the houseboats phyto-remediating plants work to clean the soil. De Ceuvel is not only a “forbidden garden” which will leave behind cleaner soil, but also a playground for sustainable technologies with creative workspaces, a cultural venue, a sustainable café, spaces to rent, and a floating bed & breakfast. Through experimentation, they are as energy self-sufficient as possible and process their own waste in new, innovative ways.

- Do a [kickbox](https://radar.squat.net/en/node/65692) or a [yoga](https://radar.squat.net/en/amsterdam/yogayaya) lesson with Krullie.

- Enjoy a kapsalon in one of the [vegan junk food bar](https://www.veganjunkfoodbar.com/) of the city. Kapsalon means hairdresser, the story behind the name of this dish comes from a special request from a customer in a regular snack in Rotterdam. This man, who was an hairdresser, asked for a special fries tray with meat, cheese, sauce and lettuce. Then, the day after he sent one of his employees to the snack to get another "hairdresser tray". This dish has become more and more popular and now it is in the menu of most of Dutch snacks

- Bike around Amsterdam and visit the neighboring cities with the network of [knooppunt](https://www.openstreetmap.org/#map=11/52.3353/4.8251&layers=C). You will pass by very nice landscapes just by keeping in mind a series of numbers (roads are very well indicated).

- Listen to [Red Light Radio](http://redlightradio.net/), an online radio station and international music platform based in Amsterdam’s Red Light District. They broadcast daily shows of local and international artists, do parties & concerts and collaborations with festivals, museums & other cultural partners all over the world.
